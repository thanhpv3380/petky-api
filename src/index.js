const express = require('express');
const path = require('path');
const compression = require('compression');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');

const camelCaseReq = require('./middlewares/camelCaseReq');
const omitReq = require('./middlewares/omitReq');
const snakeCaseRes = require('./middlewares/snakeCaseRes');
const errorHandler = require('./middlewares/errorHandler');

const { scheduleCategoriesDefault } = require('./data');
const scheduleCategoryDao = require('./daos/scheduleCategory');
const scheduleDao = require('./daos/schedule');
const scheduleJobDao = require('./daos/scheduleJob');
const taskDao = require('./daos/task');

require('dotenv').config();
require('./models').sequelize.sync();

const { PORT, NODE_ENV } = require('./configs');
const { destroyCronJob, cronJobTask } = require('./utils/cron');

const app = express();

if (NODE_ENV !== 'production') {
  const swagger = require('./swagger');
  swagger.createDocument();
}

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(camelCaseReq);
app.use(omitReq);
app.use(snakeCaseRes());
app.use(express.static(path.join(__dirname, '..', 'public')));

require('./routes')(app);

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

saveDataDefault();
async function saveDataDefault() {
  const scDefault = await scheduleCategoryDao.findByCondition({
    isDefault: true,
  });
  if (!scDefault) {
    await Promise.all(
      scheduleCategoriesDefault.map(async (scd) => {
        const scheduleCategory = await scheduleCategoryDao.create({
          label: scd.label,
          bgImage: scd.bgImage,
          isDefault: true,
        });
        await Promise.all(
          scd.schedules.map(async (sd) => {
            const schedule = await scheduleDao.create({
              label: sd.label,
              bgImage: sd.bgImage,
              isDefault: true,
              scheduleCategoryId: scheduleCategory.id,
            });
            await Promise.all(
              sd.scheduleJobs.map(async (sj) => {
                await scheduleJobDao.create({
                  label: sj.label,
                  icon: sj.icon,
                  bgColor: sj.bgColor,
                  scheduleId: schedule.id,
                  isDefault: true,
                  scheduleCategoryId: scheduleCategory.id,
                });
              }),
            );
          }),
        );
      }),
    );
  }
}

handleNotificationTask();
async function handleNotificationTask() {
  const { items: tasks } = await taskDao.findAll({});
  await Promise.all(
    tasks.map(async (task) => {
      destroyCronJob(task.id);
      cronJobTask(task);
    }),
  );
}
