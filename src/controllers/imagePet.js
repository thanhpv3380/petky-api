const imagePetService = require('../services/imagePet');

const findAllByPet = async (req, res) => {
  const { pet } = req;
  const { search, sort, fields, page, size } = req.query;
  const data = await imagePetService.findAll({
    query: { petId: pet.id },
    search,
    sort,
    fields,
    page,
    size,
  });
  return res.send({ status: 1, result: data });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const imagePet = await imagePetService.findById(+id);
  return res.send({ status: 1, result: { imagePet } });
};

const create = async (req, res) => {
  const { pet } = req;
  const payload = req.body;
  const imagePet = await imagePetService.create(pet.id, payload);
  res.send({ status: 1, result: { imagePet } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await imagePetService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await imagePetService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByPet, findById, create, update, remove };
