const scheduleCategoryService = require('../services/scheduleCategory');

const findAllByUser = async (req, res) => {
  const { user } = req;
  const scheduleCategories = await scheduleCategoryService.findAll({
    userId: user.id,
  });
  return res.send({ status: 1, result: { scheduleCategories } });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const scheduleCategory = await scheduleCategoryService.findById(+id);
  return res.send({ status: 1, result: { scheduleCategory } });
};

const create = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  const scheduleCategory = await scheduleCategoryService.create(
    user.id,
    payload,
  );
  res.send({ status: 1, result: { scheduleCategory } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await scheduleCategoryService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await scheduleCategoryService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByUser, findById, create, update, remove };
