const scheduleService = require('../services/schedule');
const scheduleJobService = require('../services/scheduleJob');

const findAllByUser = async (req, res) => {
  const { user } = req;
  const schedules = await scheduleService.findAll({ userId: user.id });
  return res.send({ status: 1, result: { schedules } });
};

const findAllScheduleJob = async (req, res) => {
  const { id } = req.params;
  const scheduleJobs = await scheduleJobService.findAllByScheduleId(+id);
  return res.send({ status: 1, result: { scheduleJobs } });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const schedule = await scheduleService.findById(+id);
  return res.send({ status: 1, result: { schedule } });
};

const create = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  const schedule = await scheduleService.create(user.id, payload);
  res.send({ status: 1, result: { schedule } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await scheduleService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await scheduleService.remove(+id);
  res.send({ status: 1 });
};

module.exports = {
  findAllByUser,
  findById,
  create,
  update,
  remove,
  findAllScheduleJob,
};
