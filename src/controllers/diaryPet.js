const diaryPetService = require('../services/diaryPet');

const findAllByPet = async (req, res) => {
  const { pet } = req;
  const { search, sort, fields, page, size } = req.query;
  const data = await diaryPetService.findAll({
    query: { petId: pet.id },
    search,
    sort,
    fields,
    page,
    size,
  });
  return res.send({ status: 1, result: data });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const diaryPet = await diaryPetService.findById(+id);
  return res.send({ status: 1, result: { diaryPet } });
};

const create = async (req, res) => {
  const { pet } = req;
  const payload = req.body;
  const diaryPet = await diaryPetService.create(pet.id, payload);
  res.send({ status: 1, result: { diaryPet } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await diaryPetService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await diaryPetService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByPet, findById, create, update, remove };
