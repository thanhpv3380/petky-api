const notificationService = require('../services/notification');

const create = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  const notification = await notificationService.create(user.id, payload);
  res.send({ status: 1, result: { notification } });
};

const removeAllByExpoPushToken = async (req, res) => {
  const { expoPushToken } = req.body;
  await notificationService.removeAllByExpoPushToken(expoPushToken);
  res.send({ status: 1 });
};

module.exports = { create, removeAllByExpoPushToken };
