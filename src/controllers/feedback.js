const feedbackService = require('../services/feedback');

const findAll = async (req, res) => {
  const { search, sort, fields, page, size } = req.query;
  const data = await feedbackService.findAll({
    query: {},
    search,
    sort,
    fields,
    page,
    size,
  });
  return res.send({ status: 1, result: data });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const feedback = await feedbackService.findById(+id);
  return res.send({ status: 1, result: { feedback } });
};

const create = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  const feedback = await feedbackService.create(user.id, payload);
  res.send({ status: 1, result: { feedback } });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await feedbackService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAll, findById, create, remove };
