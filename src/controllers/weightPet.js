const weightPetService = require('../services/weightPet');

const findAllByPet = async (req, res) => {
  const { pet } = req;
  const { search, sort, fields, page, size } = req.query;
  const data = await weightPetService.findAll({
    query: { petId: pet.id },
    search,
    sort,
    fields,
    page,
    size,
  });
  return res.send({ status: 1, result: data });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const weightPet = await weightPetService.findById(+id);
  return res.send({ status: 1, result: { weightPet } });
};

const create = async (req, res) => {
  const { pet } = req;
  const payload = req.body;
  const weightPet = await weightPetService.create(pet.id, payload);
  res.send({ status: 1, result: { weightPet } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await weightPetService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await weightPetService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByPet, findById, create, update, remove };
