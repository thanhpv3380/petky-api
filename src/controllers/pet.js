const petService = require('../services/pet');

const findAllByUser = async (req, res) => {
  const { user } = req;
  const pets = await petService.findAll({ userId: user.id });
  return res.send({ status: 1, result: { pets } });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const pet = await petService.findById(+id);
  return res.send({ status: 1, result: { pet } });
};

const create = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  const pet = await petService.create(user.id, payload);
  res.send({ status: 1, result: { pet } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await petService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await petService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByUser, findById, create, update, remove };
