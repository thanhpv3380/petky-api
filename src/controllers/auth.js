const authService = require('../services/auth');

const register = async (req, res) => {
  const payload = req.body;
  const { user, accessToken } = await authService.register(payload);
  return res.send({ status: 1, result: { accessToken, user } });
};

const login = async (req, res) => {
  const { email, password } = req.body;
  const { accessToken, user } = await authService.login(email, password);
  return res.send({ status: 1, result: { accessToken, user } });
};

const loginByGoogle = async (req, res) => {
  const { tokenId } = req.body;
  const { accessToken, user } = await authService.loginByGoogle(tokenId);
  return res.send({ status: 1, result: { accessToken, user } });
};

const verifyAccessToken = async (req, res) => {
  const { user } = req;
  res.send({ status: 1, result: { user } });
};

module.exports = { register, login, verifyAccessToken, loginByGoogle };
