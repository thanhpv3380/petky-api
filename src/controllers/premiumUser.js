const premiumUserService = require('../services/premiumUser');

const findAll = async (req, res) => {
  const { search, sort, fields, page, size } = req.query;
  const data = await premiumUserService.findAll({
    query: {},
    search,
    sort,
    fields,
    page,
    size,
  });
  return res.send({ status: 1, result: data });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const premiumUser = await premiumUserService.findById(+id);
  return res.send({ status: 1, result: { premiumUser } });
};

const create = async (req, res) => {
  const payload = req.body;
  const premiumUser = await premiumUserService.create(payload);
  res.send({ status: 1, result: { premiumUser } });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await premiumUserService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAll, findById, create, remove };
