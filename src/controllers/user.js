const userService = require('../services/user');
const authService = require('../services/auth');

const findAll = async (req, res) => {
  const users = await userService.findAll();
  return res.send({ status: 1, result: { users } });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const user = await userService.findById(+id);
  return res.send({ status: 1, result: { user } });
};

const findMe = async (req, res) => {
  const { user } = req;
  return res.send({ status: 1, result: { user } });
};

const update = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  await userService.update(user.id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { user } = req;
  await userService.remove(user.id);
  res.send({ status: 1 });
};

const changePassword = async (req, res) => {
  const { user } = req;
  const { oldPass, newPass } = req.body;
  await authService.changePassword(user.id, oldPass, newPass);
  res.send({ status: 1 });
};

module.exports = { findAll, findById, findMe, update, remove, changePassword };
