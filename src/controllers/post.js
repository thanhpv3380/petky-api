const postService = require('../services/post');

const findAll = async (req, res) => {
  const posts = await postService.findAll();
  return res.send({ status: 1, result: { posts } });
};

const create = async (req, res) => {
  const payload = req.body;
  const post = await postService.create(payload);
  res.send({ status: 1, result: { post } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await postService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await postService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAll, create, update, remove };
