const taskService = require('../services/task');

const findAllByPet = async (req, res) => {
  const { pet } = req;
  const { search, sort, fields, page, size } = req.query;
  const data = await taskService.findAll({
    query: { petId: pet.id },
    search,
    sort,
    fields,
    page,
    size,
  });
  return res.send({ status: 1, result: data });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const task = await taskService.findById(+id);
  return res.send({ status: 1, result: { task } });
};

const create = async (req, res) => {
  const { pet, user } = req;
  const payload = req.body;
  const task = await taskService.create(pet.id, payload, user.expoPushToken);
  res.send({ status: 1, result: { task } });
};

const update = async (req, res) => {
  const { user } = req;
  const { id } = req.params;
  const payload = req.body;
  await taskService.update(+id, payload, user.expoPushToken);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await taskService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByPet, findById, create, update, remove };
