const scheduleJobService = require('../services/scheduleJob');

const findAllByUser = async (req, res) => {
  const { user } = req;
  const scheduleJobs = await scheduleJobService.findAll({ userId: user.id });
  return res.send({ status: 1, result: { scheduleJobs } });
};

const findById = async (req, res) => {
  const { id } = req.params;
  const scheduleJob = await scheduleJobService.findById(+id);
  return res.send({ status: 1, result: { scheduleJob } });
};

const create = async (req, res) => {
  const { user } = req;
  const payload = req.body;
  const { schedule, scheduleJob } = await scheduleJobService.create(
    user.id,
    payload,
  );
  res.send({ status: 1, result: { scheduleJob, schedule } });
};

const update = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  await scheduleJobService.update(+id, payload);
  res.send({ status: 1 });
};

const remove = async (req, res) => {
  const { id } = req.params;
  await scheduleJobService.remove(+id);
  res.send({ status: 1 });
};

module.exports = { findAllByUser, findById, create, update, remove };
