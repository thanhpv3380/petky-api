const { DiaryPet } = require('../models');
const { findAll: findAllPagination } = require('../utils/db');

const findAll = async ({
  query,
  search,
  sort,
  fields,
  page,
  size,
  searchFields,
}) => {
  const data = await findAllPagination(DiaryPet, searchFields, {
    search,
    query,
    page,
    size,
    fields,
    sort,
  });
  return data;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const diaryPet = await DiaryPet.findOne({
      where: { id: condition },
    });
    return diaryPet && diaryPet.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const diaryPet = await DiaryPet.findOne({
      where: { ...condition },
    });
    return diaryPet && diaryPet.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const diaryPet = await DiaryPet.create(data);
  return diaryPet && diaryPet.get({ plain: true });
};

const update = async (id, data) => {
  await DiaryPet.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await DiaryPet.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
