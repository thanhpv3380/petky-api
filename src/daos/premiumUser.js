const { PremiumUser } = require('../models');
const { findAll: findAllPagination } = require('../utils/db');

const findAll = async ({
  query,
  search,
  sort,
  fields,
  page,
  size,
  searchFields,
}) => {
  const data = await findAllPagination(PremiumUser, searchFields, {
    search,
    query,
    page,
    size,
    fields,
    sort,
  });
  return data;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const premiumUser = await PremiumUser.findOne({
      where: { id: condition },
    });
    return premiumUser && premiumUser.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const premiumUser = await PremiumUser.findOne({
      where: { ...condition },
    });
    return premiumUser && premiumUser.get({ plain: true });
  }
  return null;
};

const create = async (data) => {
  const premiumUser = await PremiumUser.create(data);
  return premiumUser && premiumUser.get({ plain: true });
};

const remove = async (id) => {
  await PremiumUser.destroy({ where: { id } });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  remove,
};
