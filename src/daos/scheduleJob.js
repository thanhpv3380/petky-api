const { Op } = require('sequelize');
const { ScheduleJob } = require('../models');

const findAll = async ({ userId }) => {
  const scheduleJobs = await ScheduleJob.findAll({
    where: {
      [Op.or]: [{ userId }, { isDefault: true }],
    },
    raw: true,
  });
  return scheduleJobs;
};

const findAllByScheduleId = async (scheduleId) => {
  const scheduleJobs = await ScheduleJob.findAll({
    where: {
      scheduleId,
    },
    raw: true,
  });
  return scheduleJobs;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const scheduleJob = await ScheduleJob.findOne({
      where: { id: condition },
    });
    return scheduleJob && scheduleJob.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const scheduleJob = await ScheduleJob.findOne({
      where: { ...condition },
    });
    return scheduleJob && scheduleJob.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const { scheduleJob } = await ScheduleJob.create(data);
  return scheduleJob && scheduleJob.get({ plain: true });
};

const update = async (id, data) => {
  await ScheduleJob.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await ScheduleJob.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
  findAllByScheduleId,
};
