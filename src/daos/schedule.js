const { Op } = require('sequelize');
const { Schedule } = require('../models');

const findAll = async ({ userId }) => {
  const schedules = await Schedule.findAll({
    where: {
      [Op.or]: [{ userId }, { isDefault: true }],
    },
    raw: true,
  });
  return schedules;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const schedule = await Schedule.findOne({
      where: { id: condition },
    });
    return schedule && schedule.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const schedule = await Schedule.findOne({
      where: { ...condition },
    });
    return schedule && schedule.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const schedule = await Schedule.create(data);
  return schedule && schedule.get({ plain: true });
};

const update = async (id, data) => {
  await Schedule.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await Schedule.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
