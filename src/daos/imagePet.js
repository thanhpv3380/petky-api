const { ImagePet } = require('../models');
const { findAll: findAllPagination } = require('../utils/db');

const findAll = async ({
  query,
  search,
  sort,
  fields,
  page,
  size,
  searchFields,
}) => {
  const data = await findAllPagination(ImagePet, searchFields, {
    search,
    query,
    page,
    size,
    fields,
    sort,
  });
  return data;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const imagePet = await ImagePet.findOne({
      where: { id: condition },
    });
    return imagePet && imagePet.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const imagePet = await ImagePet.findOne({
      where: { ...condition },
    });
    return imagePet && imagePet.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const imagePet = await ImagePet.create(data);
  return imagePet && imagePet.get({ plain: true });
};

const update = async (id, data) => {
  await ImagePet.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await ImagePet.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
