const { Task, ScheduleJob } = require('../models');
const { findAll: findAllPagination } = require('../utils/db');

const findAll = async ({
  query,
  search,
  sort,
  fields,
  page,
  size,
  searchFields,
}) => {
  const data = await findAllPagination(Task, searchFields, {
    search,
    query,
    page,
    size,
    fields,
    sort,
    include: [
      {
        model: ScheduleJob,
        as: 'ScheduleJob',
        attributes: { exclude: ['createdAt', 'updatedAt', 'userId'] },
      },
    ],
  });
  return data;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const task = await Task.findOne({
      where: { id: condition },
    });
    return task && task.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const task = await Task.findOne({
      where: { ...condition },
    });
    return task && task.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const task = await Task.create(data);
  return task && task.get({ plain: true });
};

const update = async (id, data) => {
  await Task.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await Task.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
