const { User } = require('../models');

const findAllUser = async ({ query }) => {
  const users = await User.findAll({
    where: query && { ...query },
    raw: true,
  });
  return users;
};

const createUser = async (data) => {
  const user = await User.create(data);
  return user && user.get({ plain: true });
};

const findUser = async (condition) => {
  if (typeof condition === 'number') {
    const user = await User.findOne({
      where: { id: condition },
    });
    return user && user.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const user = await User.findOne({ where: { ...condition } });
    return user && user.get({ plain: true });
  }

  return null;
};

const updateUserByCondition = async (condition, data) => {
  await User.update(data, {
    where: { ...condition },
  });
};

const updateUser = async (userId, data) => {
  await User.update(data, {
    where: { id: userId },
  });
};

const deleteUser = async (userId) => {
  await User.destroy({
    where: {
      id: userId,
    },
  });
};

module.exports = {
  findAllUser,
  createUser,
  findUser,
  updateUser,
  deleteUser,
  updateUserByCondition,
};
