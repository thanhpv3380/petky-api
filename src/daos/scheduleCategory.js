const { Op } = require('sequelize');
const { ScheduleCategory } = require('../models');

const findAll = async ({ userId }) => {
  const scheduleCategories = await ScheduleCategory.findAll({
    where: {
      [Op.or]: [{ userId }, { isDefault: true }],
    },
    raw: true,
  });
  return scheduleCategories;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const scheduleCategory = await ScheduleCategory.findOne({
      where: { id: condition },
    });
    return scheduleCategory && scheduleCategory.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const scheduleCategory = await ScheduleCategory.findOne({
      where: { ...condition },
    });
    return scheduleCategory && scheduleCategory.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const scheduleCategory = await ScheduleCategory.create(data);
  return scheduleCategory && scheduleCategory.get({ plain: true });
};

const update = async (id, data) => {
  await ScheduleCategory.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await ScheduleCategory.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
