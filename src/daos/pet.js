const { Pet } = require('../models');

const findAll = async ({ query }) => {
  const pets = await Pet.findAll({
    where: query && { ...query },
    raw: true,
  });
  return pets;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const pet = await Pet.findOne({
      where: { id: condition },
    });
    return pet && pet.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const pet = await Pet.findOne({ where: { ...condition } });
    return pet && pet.get({ plain: true });
  }

  return null;
};

const create = async ({ userId, ...data }) => {
  const pet = await Pet.create({
    userId,
    ...data,
  });
  return pet && pet.get({ plain: true });
};

const update = async (id, data) => {
  await Pet.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await Pet.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
