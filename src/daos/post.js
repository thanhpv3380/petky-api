const { Post } = require('../models');

const findAll = async () => {
  const Posts = await Post.findAll({
    order: [['createdAt', 'DESC']],
    raw: true,
  });
  return Posts;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const post = await Post.findOne({
      where: { id: condition },
    });
    return post && post.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const post = await Post.findOne({ where: { ...condition } });
    return post && post.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const post = await Post.create(data);
  return post && post.get({ plain: true });
};

const update = async (id, data) => {
  await Post.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await Post.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
