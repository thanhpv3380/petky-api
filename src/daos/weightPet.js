const { WeightPet } = require('../models');
const { findAll: findAllPagination } = require('../utils/db');

const findAll = async ({
  query,
  search,
  sort,
  fields,
  page,
  size,
  searchFields,
}) => {
  const data = await findAllPagination(WeightPet, searchFields, {
    search,
    query,
    page,
    size,
    fields,
    sort,
  });
  return data;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const weightPet = await WeightPet.findOne({
      where: { id: condition },
    });
    return weightPet && weightPet.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const weightPet = await WeightPet.findOne({
      where: { ...condition },
    });
    return weightPet && weightPet.get({ plain: true });
  }

  return null;
};

const create = async (data) => {
  const weightPet = await WeightPet.create(data);
  return weightPet && weightPet.get({ plain: true });
};

const update = async (id, data) => {
  await WeightPet.update(data, {
    where: { id },
  });
};

const remove = async (id) => {
  await WeightPet.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  remove,
};
