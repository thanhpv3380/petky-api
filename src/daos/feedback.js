const { Feedback } = require('../models');
const { findAll: findAllPagination } = require('../utils/db');

const findAll = async ({
  query,
  search,
  sort,
  fields,
  page,
  size,
  searchFields,
}) => {
  const data = await findAllPagination(Feedback, searchFields, {
    search,
    query,
    page,
    size,
    fields,
    sort,
  });
  return data;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const feedback = await Feedback.findOne({
      where: { id: condition },
    });
    return feedback && feedback.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const feedback = await Feedback.findOne({
      where: { ...condition },
    });
    return feedback && feedback.get({ plain: true });
  }
  return null;
};

const create = async (data) => {
  const feedback = await Feedback.create(data);
  return feedback && feedback.get({ plain: true });
};

const remove = async (id) => {
  await Feedback.destroy({
    where: {
      id,
    },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  remove,
};
