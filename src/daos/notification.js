const { Notification } = require('../models');

const findAll = async ({ query }) => {
  const notifications = await Notification.findAll({
    where: query && { ...query },
    raw: true,
  });
  return notifications;
};

const findByCondition = async (condition) => {
  if (typeof condition === 'number') {
    const notification = await Notification.findOne({
      where: { id: condition },
    });
    return notification && notification.get({ plain: true });
  }

  if (typeof condition === 'object' && condition !== null) {
    const notification = await Notification.findOne({
      where: { ...condition },
    });
    return notification && notification.get({ plain: true });
  }

  return null;
};

const create = async ({ userId, ...data }) => {
  const notification = await Notification.create({
    userId,
    ...data,
  });
  return notification && notification.get({ plain: true });
};

const update = async (id, data) => {
  await Notification.update(data, {
    where: { id },
  });
};

const removeByCondition = async (condition) => {
  await Notification.destroy({
    where: { ...condition },
  });
};

module.exports = {
  findAll,
  findByCondition,
  create,
  update,
  removeByCondition,
};
