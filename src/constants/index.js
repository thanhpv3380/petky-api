module.exports = {
  A_WEEK: 7 * 86400 * 1000,
  DESTINATION: 'public',
  IMAGES_FOLDER: 'images',
  AUDIO_FOLDER: 'audios',
  VIDEO_FOLDER: 'videos',
  OTHER_FOLDER: 'others',
};
