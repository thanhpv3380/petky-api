const asyncMiddleware = require('./async');
const CustomError = require('../errors/CustomError');
const codes = require('../errors/code');
const userTypes = require('../enums/user');

const addScheduleAuth = async (req, res, next) => {
  const { user } = req;

  if (user.type !== userTypes.PREMIUM) {
    throw new CustomError(codes.UNAUTHORIZED_ADD_SCHEDULE);
  }

  return next();
};

module.exports = {
  addScheduleAuth: asyncMiddleware(addScheduleAuth),
};
