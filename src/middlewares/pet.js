const asyncMiddleware = require('./async');
const CustomError = require('../errors/CustomError');
const codes = require('../errors/code');
const petService = require('../services/pet');
const userTypes = require('../enums/user');

const getPet = async (req, res, next) => {
  const { petid: petId } = req.headers;
  if (!petId) {
    throw new CustomError(codes.NOT_FOUND, 'PetId not found');
  }
  const pet = await petService.findById(+petId);

  if (!pet) {
    throw new CustomError(codes.NOT_FOUND, 'Pet not found');
  }
  req.pet = pet;

  return next();
};

const addPetAuth = async (req, res, next) => {
  const { user } = req;

  const AMOUNT_PET_LIMITED = process.env.AMOUNT_PET_LIMITED || 5;
  const MAX_PET_USER_BASIC = process.env.MAX_PET_USER_BASIC || 1;
  const pets = await petService.findAll({ userId: user.id });
  if (pets && pets.length >= AMOUNT_PET_LIMITED)
    throw new CustomError(codes.LIMIT_PET);

  if (
    pets &&
    pets.length >= MAX_PET_USER_BASIC &&
    user.type !== userTypes.PREMIUM
  ) {
    throw new CustomError(codes.UNAUTHORIZED_ADD_PET);
  }

  return next();
};

module.exports = {
  getPet: asyncMiddleware(getPet),
  addPetAuth: asyncMiddleware(addPetAuth),
};
