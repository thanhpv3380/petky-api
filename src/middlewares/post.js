const asyncMiddleware = require('./async');
const CustomError = require('../errors/CustomError');
const codes = require('../errors/code');

const checkAdmin = async (req, res, next) => {
  const { user } = req;
  if (!user.isAdmin) {
    throw new CustomError(codes.UNAUTHORIZED);
  }
  return next();
};

module.exports = {
  checkAdmin: asyncMiddleware(checkAdmin),
};
