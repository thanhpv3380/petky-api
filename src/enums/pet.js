const petGender = {
  MALE: 'MALE',
  FEMALE: 'FEMALE',
};

module.exports = {
  petGender,
};
