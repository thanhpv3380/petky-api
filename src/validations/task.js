const { Joi, validate } = require('express-validation');
const { taskTimeTypes } = require('../enums/task');

const update = {
  body: Joi.object({
    dateTime: Joi.date(),
    timeType: Joi.string()
      .trim()
      .valid(...Object.keys(taskTimeTypes).map((key) => taskTimeTypes[key])),
    pending: Joi.boolean(),
    note: Joi.string().trim().allow(null).allow(''),
    scheduleJobId: Joi.number(),
    days: Joi.string().trim(),
    weekdays: Joi.string().trim(),
  }),
};

const create = {
  body: Joi.object({
    dateTime: Joi.date().required(),
    timeType: Joi.string()
      .trim()
      .valid(...Object.keys(taskTimeTypes).map((key) => taskTimeTypes[key]))
      .required(),
    pending: Joi.boolean(),
    note: Joi.string().trim().allow(null).allow(''),
    scheduleJobId: Joi.number().required(),
    days: Joi.string().trim(),
    weekdays: Joi.string().trim(),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
  updateValidate: validate(update, { keyByField: true }),
};
