const { Joi, validate } = require('express-validation');

const update = {
  body: Joi.object({
    imageUrl: Joi.string().trim(),
    link: Joi.string().trim(),
  }),
};

const create = {
  body: Joi.object({
    imageUrl: Joi.string().trim().required(),
    link: Joi.string().trim().required(),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
  updateValidate: validate(update, { keyByField: true }),
};
