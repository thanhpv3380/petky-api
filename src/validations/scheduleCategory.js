const { Joi, validate } = require('express-validation');

const update = {
  body: Joi.object({
    label: Joi.string().trim(),
    bgImage: Joi.string().trim().allow(null).allow(''),
  }),
};

const create = {
  body: Joi.object({
    label: Joi.string().trim().required(),
    bgImage: Joi.string().trim().allow(null).allow(''),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
  updateValidate: validate(update, { keyByField: true }),
};
