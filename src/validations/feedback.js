const { Joi, validate } = require('express-validation');

const create = {
  body: Joi.object({
    content: Joi.string().trim().required(),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
};
