const { Joi, validate } = require('express-validation');

const create = {
  body: Joi.object({
    phoneNumber: Joi.string()
      .trim()
      .length(10)
      .pattern(/^[0-9]+$/)
      .required(),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
};
