const { Joi, validate } = require('express-validation');
const { petGender } = require('../enums/pet');

const update = {
  body: Joi.object({
    name: Joi.string().trim(),
    age: Joi.number().min(1).allow(null),
    gender: Joi.string()
      .trim()
      .valid(...Object.keys(petGender).map((key) => petGender[key]))
      .allow(null),
    avatar: Joi.string().trim().allow(null).allow(''),
    species: Joi.string().trim().allow(null).allow(''),
  }),
};

const create = {
  body: Joi.object({
    name: Joi.string().trim().required(),
    age: Joi.number().min(1).allow(null),
    weight: Joi.number().min(0).allow(null),
    gender: Joi.string()
      .trim()
      .valid(...Object.keys(petGender).map((key) => petGender[key]))
      .allow(null),
    avatar: Joi.string().trim().allow(null).allow(''),
    species: Joi.string().trim().allow(null).allow(''),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
  updateValidate: validate(update, { keyByField: true }),
};
