const { Joi, validate } = require('express-validation');
const userTypes = require('../enums/user');

const updateProfile = {
  body: Joi.object({
    name: Joi.string().trim(),
    phoneNumber: Joi.string().trim().allow(null).allow(''),
    address: Joi.string().trim().allow(null).allow(''),
    avatar: Joi.string().trim().allow(null).allow(''),
    type: Joi.string()
      .trim()
      .valid(...Object.keys(userTypes).map((key) => userTypes[key])),
    expoPushToken: Joi.string().trim(),
  }),
};

const changePassword = {
  body: Joi.object({
    oldPass: Joi.string().trim().required(),
    newPass: Joi.string().trim().required(),
  }),
};

module.exports = {
  updateProfileValidate: validate(updateProfile, { keyByField: true }),
  changePasswordValidate: validate(changePassword, { keyByField: true }),
};
