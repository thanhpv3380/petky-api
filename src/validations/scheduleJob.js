const { Joi, validate } = require('express-validation');

const update = {
  body: Joi.object({
    label: Joi.string().trim(),
    bgImage: Joi.string().trim().allow(null).allow(''),
    icon: Joi.string().trim().allow(null).allow(''),
    bgColor: Joi.string().trim().allow(null).allow(''),
    scheduleId: Joi.number(),
  }),
};

const create = {
  body: Joi.object({
    label: Joi.string().trim().required(),
    bgImage: Joi.string().trim().allow(null).allow(''),
    icon: Joi.string().trim().allow(null).allow(''),
    bgColor: Joi.string().trim().allow(null).allow(''),
    scheduleCategoryId: Joi.number().required(),
  }),
};

module.exports = {
  createValidate: validate(create, { keyByField: true }),
  updateValidate: validate(update, { keyByField: true }),
};
