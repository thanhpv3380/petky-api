const { Joi, validate } = require('express-validation');

const login = {
  body: Joi.object({
    email: Joi.string().email().trim().required(),
    password: Joi.string().trim().required(),
  }),
};

const loginByGoogle = {
  body: Joi.object({
    tokenId: Joi.string().trim().required(),
  }),
};

const register = {
  body: Joi.object({
    email: Joi.string().email().trim().required(),
    name: Joi.string().trim().required(),
    password: Joi.string().trim().required(),
    phoneNumber: Joi.string()
      .trim()
      .length(10)
      .pattern(/^[0-9]+$/)
      .required(),
    address: Joi.string().trim().allow(null).allow(''),
    avatar: Joi.string().trim().allow(null).allow(''),
  }),
};

module.exports = {
  loginByGoogleValidate: validate(loginByGoogle, { keyByField: true }),
  loginValidate: validate(login, { keyByField: true }),
  registerValidate: validate(register, { keyByField: true }),
};
