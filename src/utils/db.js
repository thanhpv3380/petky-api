/* eslint-disable no-nested-ternary */
const db = require('../models');

const { Op } = db.Sequelize;

const getPagination = (page, size) => {
  const limit = size ? +size : null;
  const offset = limit ? (page ? page * limit : 0) : null;

  return { limit, offset };
};

const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows: items } = data;
  const currentPage = page ? +page : 0;
  const totalPages = limit
    ? Math.ceil(totalItems / limit)
    : totalItems > 0
    ? 1
    : 0;

  return { totalItems, items, totalPages, currentPage };
};

async function findAll(
  model,
  searchFields,
  { search, query, page, size, fields, sort, include },
) {
  const condition = query && typeof query === 'object' ? { ...query } : {};
  if (search && searchFields) {
    searchFields.forEach((field) => {
      condition[field] = { [Op.like]: `%${search}%` };
    });
  }

  const { limit, offset } = getPagination(page, size);

  const order =
    sort && typeof sort === 'string'
      ? sort.split(',').map((element) => {
          const field = element.substring(0, element.lastIndexOf('_'));
          const value =
            element.substring(element.lastIndexOf('_') + 1) === 'asc'
              ? 'ASC'
              : 'DESC';
          return [field, value];
        })
      : sort;

  const attributes =
    fields && typeof fields === 'string' ? fields.split(',') : fields;

  const data = await model.findAndCountAll({
    where: condition,
    limit,
    offset,
    attributes,
    order,
    raw: true,
    nest: true,
    include,
  });
  return getPagingData(data, page, limit);
}

module.exports = { findAll };
