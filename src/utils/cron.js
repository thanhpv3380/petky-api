const axios = require('axios');
const cron = require('node-cron');
const { taskTimeTypes } = require('../enums/task');

const scheduleJobDao = require('../daos/scheduleJob');
const taskDao = require('../daos/task');
const petDao = require('../daos/pet');
const notificationDao = require('../daos/notification');

const tasks = [];
const upcomingTasks = [];

const TIME_UPCOMING_DAILY = 1;
const TIME_UPCOMING_WEEKLY = 1;
const TIME_UPCOMING_MONTHLY = 1;

const EXPO_SERVER_URL = 'https://exp.host/--/api/v2/push/send';
const translates = {
  feeding: 'Cho ăn',
  exercise: 'Thể dục',
  vaccinate: 'Tiêm vaccine',
  shower: 'Tắm',
  medicalExamination: 'Khám bệnh',
  spa: 'Spa',
};

const pushNotificationTask = async (expoPushToken, task, isComing) => {
  if (expoPushToken) {
    const title = task.scheduleJob.isDefault
      ? translates[task.scheduleJob.label]
      : task.scheduleJob.label;

    const message = {
      to: expoPushToken,
      sound: 'default',
      title: isComing ? `${title} - sắp tới` : title,
      body: task.note,
      data: { taskId: task.id, petId: task.petId },
      categoryId: isComing ? 'upcomingTimeTask' : 'onTimeTask',
    };
    await axios.post(EXPO_SERVER_URL, message);
  }
};

const getTokenToPushNotification = async (task, isComing) => {
  const { userId } = await petDao.findByCondition(task.petId);
  const tokens = await notificationDao.findAll({
    query: { userId },
  });
  const dictToken = {};
  await Promise.all(
    tokens
      .filter((el) => {
        if (!dictToken[el.expoPushToken]) {
          dictToken[el.expoPushToken] = 1;
          return true;
        }
        return false;
      })
      .map(async (token) => {
        await pushNotificationTask(token.expoPushToken, task, isComing);
      }),
  );
};

const cronJobTask = async (data) => {
  const { id, timeType, dateTime, weekdays, days, scheduleJobId } = data;

  const date = new Date(dateTime);
  const hour = date.getHours();
  const minute = date.getMinutes();

  let scheduleTime = null;
  let upcomingScheduleTime = null;

  // config time cron
  switch (timeType) {
    case taskTimeTypes.DAILY: {
      scheduleTime = `${minute} ${hour} * * *`;
      upcomingScheduleTime = `${minute} ${
        hour - TIME_UPCOMING_DAILY < 0
          ? 24 + hour - TIME_UPCOMING_DAILY
          : hour - TIME_UPCOMING_DAILY
      } * * *`;
      break;
    }
    case taskTimeTypes.WEEKLY: {
      const newWeekdays = weekdays
        .split(',')
        .map((el) => {
          const num = parseInt(el, 10);
          if (num - TIME_UPCOMING_WEEKLY < 0)
            return 7 + num - TIME_UPCOMING_WEEKLY;
          return num - TIME_UPCOMING_WEEKLY;
        })
        .join(',');
      scheduleTime = `${minute} ${hour} * * ${weekdays}`;
      upcomingScheduleTime = `${minute} ${hour} * * ${newWeekdays}`;
      break;
    }
    case taskTimeTypes.MONTHLY: {
      const newDays = days
        .split(',')
        .map((el) => {
          const num = parseInt(el, 10);
          if (num - TIME_UPCOMING_MONTHLY <= 0)
            return 31 + num - TIME_UPCOMING_MONTHLY;
          return num - TIME_UPCOMING_MONTHLY;
        })
        .join(',');
      scheduleTime = `${minute} ${hour} ${days} * *`;
      upcomingScheduleTime = `${minute} ${hour} ${newDays} * *`;
      break;
    }
    default:
  }

  let scheduleJob = null;
  if (!data.scheduleJob || typeof data.scheduleJob !== 'object') {
    scheduleJob = await scheduleJobDao.findByCondition(scheduleJobId);
  } else {
    scheduleJob = { ...data.scheduleJob };
  }

  if (scheduleJob) {
    // schedule
    const schedule = cron.schedule(scheduleTime, async () => {
      await Promise.all([
        await taskDao.update(id, { pending: true }),
        await getTokenToPushNotification(
          {
            ...data,
            scheduleJob,
          },
          false,
        ),
      ]);
    });
    if (scheduleTime) {
      schedule.start();
      tasks.push({ id, cron: schedule });
    }

    if (cron.validate(upcomingScheduleTime)) {
      // upcoming schedule
      const upcomingSchedule = cron.schedule(upcomingScheduleTime, async () => {
        await getTokenToPushNotification(
          {
            ...data,
            scheduleJob,
          },
          true,
        );
      });
      if (upcomingScheduleTime) {
        upcomingSchedule.start();
        upcomingTasks.push({ id, cron: upcomingSchedule });
      }
    }
  }
};

const destroyCronJob = (id) => {
  const taskCronIndex = tasks.findIndex((el) => el.id === id);
  if (taskCronIndex >= 0) {
    tasks[taskCronIndex].cron.stop();
    tasks.splice(taskCronIndex, 1);
  }

  const upcomingTaskCronIndex = upcomingTasks.findIndex((el) => el.id === id);
  if (upcomingTaskCronIndex >= 0) {
    upcomingTasks[upcomingTaskCronIndex].cron.stop();
    upcomingTasks.splice(upcomingTaskCronIndex, 1);
  }
};

module.exports = {
  cronJobTask,
  destroyCronJob,
};
