module.exports = (sequelize, Sequelize) => {
  const ImagePet = sequelize.define('ImagePet', {
    url: {
      type: Sequelize.STRING(200),
      field: 'url',
    },
    description: {
      type: Sequelize.STRING(200),
      field: 'description',
    },
  });
  return ImagePet;
};
