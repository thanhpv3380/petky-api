const { petGender } = require('../enums/pet');

module.exports = (sequelize, Sequelize) => {
  const Pet = sequelize.define('Pet', {
    name: {
      type: Sequelize.STRING(100),
      field: 'name',
    },
    age: {
      type: Sequelize.INTEGER,
      field: 'age',
    },
    weight: {
      type: Sequelize.DECIMAL,
      field: 'weight',
    },
    gender: {
      type: Sequelize.ENUM(Object.keys(petGender).map((el) => petGender[el])),
      field: 'gender',
    },
    avatar: {
      type: Sequelize.STRING(200),
      field: 'avatar',
    },
    species: {
      type: Sequelize.STRING(200),
      field: 'species',
    },
  });
  return Pet;
};
