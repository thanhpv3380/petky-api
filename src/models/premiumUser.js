module.exports = (sequelize, Sequelize) => {
  const PremiumUser = sequelize.define('PremiumUser', {
    phoneNumber: {
      type: Sequelize.STRING(100),
      field: 'phoneNumber',
    },
  });
  return PremiumUser;
};
