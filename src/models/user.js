const userTypes = require('../enums/user');

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define('User', {
    name: {
      type: Sequelize.STRING(100),
      field: 'name',
    },
    email: {
      type: Sequelize.STRING(100),
      field: 'email',
    },
    password: {
      type: Sequelize.STRING(100),
      field: 'password',
    },
    phoneNumber: {
      type: Sequelize.STRING(100),
      field: 'phoneNumber',
    },
    address: {
      type: Sequelize.STRING(100),
      field: 'address',
    },
    avatar: {
      type: Sequelize.STRING(200),
      field: 'avatar',
    },
    type: {
      type: Sequelize.ENUM(Object.keys(userTypes).map((el) => userTypes[el])),
      defaultValue: userTypes.BASIC,
      field: 'type',
    },
    isAdmin: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      field: 'isAdmin',
    },
  });
  return User;
};
