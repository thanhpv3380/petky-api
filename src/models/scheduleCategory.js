module.exports = (sequelize, Sequelize) => {
  const ScheduleCategory = sequelize.define('ScheduleCategory', {
    label: {
      type: Sequelize.STRING(100),
      field: 'label',
    },
    bgImage: {
      type: Sequelize.STRING(100),
      field: 'bgImage',
    },
    isDefault: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      field: 'isDefault',
    },
  });
  return ScheduleCategory;
};
