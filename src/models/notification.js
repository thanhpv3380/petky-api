module.exports = (sequelize, Sequelize) => {
  const Notification = sequelize.define('Notification', {
    expoPushToken: {
      type: Sequelize.STRING(200),
      field: 'expoPushToken',
    },
  });
  return Notification;
};
