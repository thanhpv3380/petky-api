module.exports = (sequelize, Sequelize) => {
  const Feedback = sequelize.define('Feedback', {
    content: {
      type: Sequelize.STRING(200),
      field: 'content',
    },
  });
  return Feedback;
};
