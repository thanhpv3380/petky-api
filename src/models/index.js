/* eslint-disable import/no-dynamic-require */
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const exceptFiles = ['index.js', 'config.js'];
const config = require('../configs');

const db = {};

const sequelize = new Sequelize(
  config.MYSQL.database,
  config.MYSQL.username,
  config.MYSQL.password,
  config.MYSQL,
);

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf('.') !== 0 &&
      exceptFiles.indexOf(file) < 0 &&
      file.slice(-3) === '.js'
    );
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(
      sequelize,
      Sequelize.DataTypes,
    );
    db[model.name] = model;
  });

// user - notification
db.User.hasMany(db.Notification, {
  foreignKey: 'userId',
  onDelete: 'cascade',
});
db.Notification.belongsTo(db.User, {
  foreignKey: 'userId',
});

// user - pet
db.User.hasMany(db.Pet, {
  foreignKey: 'userId',
  onDelete: 'cascade',
});
db.Pet.belongsTo(db.User, {
  foreignKey: 'userId',
});

// user - scheduleCategory
db.User.hasMany(db.ScheduleCategory, {
  foreignKey: 'userId',
  onDelete: 'cascade',
});
db.ScheduleCategory.belongsTo(db.User, {
  foreignKey: 'userId',
});

// user - schedule
db.User.hasMany(db.Schedule, {
  foreignKey: 'userId',
  onDelete: 'cascade',
});
db.Schedule.belongsTo(db.User, {
  foreignKey: 'userId',
});

// user - scheduleJob
db.User.hasMany(db.ScheduleJob, {
  foreignKey: 'userId',
  onDelete: 'cascade',
});
db.ScheduleJob.belongsTo(db.User, {
  foreignKey: 'userId',
});

// scheduleCategory - schedule
db.ScheduleCategory.hasMany(db.Schedule, {
  foreignKey: 'scheduleCategoryId',
  onDelete: 'cascade',
});
db.Schedule.belongsTo(db.ScheduleCategory, {
  foreignKey: 'scheduleCategoryId',
});

// schedule - scheduleJob
db.Schedule.hasMany(db.ScheduleJob, {
  foreignKey: 'scheduleId',
  onDelete: 'cascade',
});
db.ScheduleJob.belongsTo(db.Schedule, {
  foreignKey: 'scheduleId',
});

// scheduleCategory - scheduleJob
db.ScheduleCategory.hasMany(db.ScheduleJob, {
  foreignKey: 'scheduleCategoryId',
  onDelete: 'cascade',
});
db.ScheduleJob.belongsTo(db.ScheduleCategory, {
  foreignKey: 'scheduleCategoryId',
});

// ScheduleJob - Task
db.Task.belongsTo(db.ScheduleJob, {
  foreignKey: 'scheduleJobId',
  onDelete: 'cascade',
});

// Pet - Task
db.Pet.hasMany(db.Task, {
  foreignKey: 'petId',
  onDelete: 'cascade',
});
db.Task.belongsTo(db.Pet, {
  foreignKey: 'petId',
});

// Pet - Diary
db.Pet.hasMany(db.DiaryPet, {
  foreignKey: 'petId',
  onDelete: 'cascade',
});
db.DiaryPet.belongsTo(db.Pet, {
  foreignKey: 'petId',
});

// Pet - Image
db.Pet.hasMany(db.ImagePet, {
  foreignKey: 'petId',
  onDelete: 'cascade',
});
db.ImagePet.belongsTo(db.Pet, {
  foreignKey: 'petId',
});

// Pet - Weight
db.Pet.hasMany(db.WeightPet, {
  foreignKey: 'petId',
  onDelete: 'cascade',
});
db.WeightPet.belongsTo(db.Pet, {
  foreignKey: 'petId',
});

// user - feedback
db.User.hasMany(db.Feedback, {
  foreignKey: 'userId',
  onDelete: 'cascade',
});
db.Feedback.belongsTo(db.User, {
  foreignKey: 'userId',
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
