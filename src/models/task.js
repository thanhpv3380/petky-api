const { taskTimeTypes } = require('../enums/task');

module.exports = (sequelize, Sequelize) => {
  const Task = sequelize.define('Task', {
    timeType: {
      type: Sequelize.ENUM(
        Object.keys(taskTimeTypes).map((el) => taskTimeTypes[el]),
      ),
      field: 'timeType',
    },
    dateTime: {
      type: Sequelize.DATE,
      field: 'dateTime',
    },
    weekdays: {
      type: Sequelize.STRING,
      field: 'weekdays',
    },
    days: {
      type: Sequelize.STRING(200),
      field: 'days',
    },
    pending: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      field: 'pending',
    },
    note: {
      type: Sequelize.STRING(200),
      field: 'note',
    },
  });
  return Task;
};
