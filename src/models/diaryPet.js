module.exports = (sequelize, Sequelize) => {
  const DiaryPet = sequelize.define('DiaryPet', {
    content: {
      type: Sequelize.STRING(200),
      field: 'content',
    },
  });
  return DiaryPet;
};
