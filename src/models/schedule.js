module.exports = (sequelize, Sequelize) => {
  const Schedule = sequelize.define('Schedule', {
    label: {
      type: Sequelize.STRING(100),
      field: 'label',
    },
    bgImage: {
      type: Sequelize.STRING(100),
      field: 'bgImage',
    },
    isDefault: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      field: 'isDefault',
    },
  });
  return Schedule;
};
