'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "Post", deps: []
 * createTable "PremiumUser", deps: []
 * createTable "User", deps: []
 * createTable "Pet", deps: [User]
 * createTable "Feedback", deps: [User]
 * createTable "DiaryPet", deps: [Pet]
 * createTable "Notification", deps: [User]
 * createTable "ImagePet", deps: [Pet]
 * createTable "ScheduleCategory", deps: [User]
 * createTable "Schedule", deps: [User, ScheduleCategory]
 * createTable "ScheduleJob", deps: [User, Schedule, ScheduleCategory]
 * createTable "Task", deps: [ScheduleJob, Pet]
 * createTable "WeightPet", deps: [Pet]
 *
 **/

var info = {
    "revision": 1,
    "name": "noname",
    "created": "2022-01-10T16:45:06.397Z",
    "comment": ""
};

var migrationCommands = function(transaction) {
    return [{
            fn: "createTable",
            params: [
                "Post",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "imageUrl": {
                        "type": Sequelize.STRING(100),
                        "field": "imageUrl"
                    },
                    "link": {
                        "type": Sequelize.STRING(100),
                        "field": "link"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "PremiumUser",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "phoneNumber": {
                        "type": Sequelize.STRING(100),
                        "field": "phoneNumber"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "User",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "name": {
                        "type": Sequelize.STRING(100),
                        "field": "name"
                    },
                    "email": {
                        "type": Sequelize.STRING(100),
                        "field": "email"
                    },
                    "password": {
                        "type": Sequelize.STRING(100),
                        "field": "password"
                    },
                    "phoneNumber": {
                        "type": Sequelize.STRING(100),
                        "field": "phoneNumber"
                    },
                    "address": {
                        "type": Sequelize.STRING(100),
                        "field": "address"
                    },
                    "avatar": {
                        "type": Sequelize.STRING(200),
                        "field": "avatar"
                    },
                    "type": {
                        "type": Sequelize.ENUM('BASIC', 'PREMIUM'),
                        "field": "type",
                        "defaultValue": "BASIC"
                    },
                    "isAdmin": {
                        "type": Sequelize.BOOLEAN,
                        "field": "isAdmin",
                        "defaultValue": false
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "Pet",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "name": {
                        "type": Sequelize.STRING(100),
                        "field": "name"
                    },
                    "age": {
                        "type": Sequelize.INTEGER,
                        "field": "age"
                    },
                    "weight": {
                        "type": Sequelize.DECIMAL,
                        "field": "weight"
                    },
                    "gender": {
                        "type": Sequelize.ENUM('MALE', 'FEMALE'),
                        "field": "gender"
                    },
                    "avatar": {
                        "type": Sequelize.STRING(200),
                        "field": "avatar"
                    },
                    "species": {
                        "type": Sequelize.STRING(200),
                        "field": "species"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "userId": {
                        "type": Sequelize.INTEGER,
                        "field": "userId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "User",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "Feedback",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "content": {
                        "type": Sequelize.STRING(200),
                        "field": "content"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "userId": {
                        "type": Sequelize.INTEGER,
                        "field": "userId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "User",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "DiaryPet",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "content": {
                        "type": Sequelize.STRING(200),
                        "field": "content"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "petId": {
                        "type": Sequelize.INTEGER,
                        "field": "petId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "Pet",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "Notification",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "expoPushToken": {
                        "type": Sequelize.STRING(200),
                        "field": "expoPushToken"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "userId": {
                        "type": Sequelize.INTEGER,
                        "field": "userId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "User",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "ImagePet",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "url": {
                        "type": Sequelize.STRING(200),
                        "field": "url"
                    },
                    "description": {
                        "type": Sequelize.STRING(200),
                        "field": "description"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "petId": {
                        "type": Sequelize.INTEGER,
                        "field": "petId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "Pet",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "ScheduleCategory",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "label": {
                        "type": Sequelize.STRING(100),
                        "field": "label"
                    },
                    "bgImage": {
                        "type": Sequelize.STRING(100),
                        "field": "bgImage"
                    },
                    "isDefault": {
                        "type": Sequelize.BOOLEAN,
                        "field": "isDefault",
                        "defaultValue": false
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "userId": {
                        "type": Sequelize.INTEGER,
                        "field": "userId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "User",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "Schedule",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "label": {
                        "type": Sequelize.STRING(100),
                        "field": "label"
                    },
                    "bgImage": {
                        "type": Sequelize.STRING(100),
                        "field": "bgImage"
                    },
                    "isDefault": {
                        "type": Sequelize.BOOLEAN,
                        "field": "isDefault",
                        "defaultValue": false
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "userId": {
                        "type": Sequelize.INTEGER,
                        "field": "userId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "User",
                            "key": "id"
                        },
                        "allowNull": true
                    },
                    "scheduleCategoryId": {
                        "type": Sequelize.INTEGER,
                        "field": "scheduleCategoryId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "ScheduleCategory",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "ScheduleJob",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "label": {
                        "type": Sequelize.STRING(100),
                        "field": "label"
                    },
                    "icon": {
                        "type": Sequelize.STRING(100),
                        "field": "icon"
                    },
                    "bgColor": {
                        "type": Sequelize.STRING(100),
                        "field": "bgColor"
                    },
                    "isDefault": {
                        "type": Sequelize.BOOLEAN,
                        "field": "isDefault",
                        "defaultValue": false
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "userId": {
                        "type": Sequelize.INTEGER,
                        "field": "userId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "User",
                            "key": "id"
                        },
                        "allowNull": true
                    },
                    "scheduleId": {
                        "type": Sequelize.INTEGER,
                        "field": "scheduleId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "Schedule",
                            "key": "id"
                        },
                        "allowNull": true
                    },
                    "scheduleCategoryId": {
                        "type": Sequelize.INTEGER,
                        "field": "scheduleCategoryId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "ScheduleCategory",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "Task",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "timeType": {
                        "type": Sequelize.ENUM('DAILY', 'WEEKLY', 'MONTHLY', 'ANNUALLY'),
                        "field": "timeType"
                    },
                    "dateTime": {
                        "type": Sequelize.DATE,
                        "field": "dateTime"
                    },
                    "weekdays": {
                        "type": Sequelize.STRING,
                        "field": "weekdays"
                    },
                    "days": {
                        "type": Sequelize.STRING(200),
                        "field": "days"
                    },
                    "pending": {
                        "type": Sequelize.BOOLEAN,
                        "field": "pending",
                        "defaultValue": false
                    },
                    "note": {
                        "type": Sequelize.STRING(200),
                        "field": "note"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "scheduleJobId": {
                        "type": Sequelize.INTEGER,
                        "field": "scheduleJobId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "ScheduleJob",
                            "key": "id"
                        },
                        "allowNull": true
                    },
                    "petId": {
                        "type": Sequelize.INTEGER,
                        "field": "petId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "Pet",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        },
        {
            fn: "createTable",
            params: [
                "WeightPet",
                {
                    "id": {
                        "type": Sequelize.INTEGER,
                        "field": "id",
                        "autoIncrement": true,
                        "primaryKey": true,
                        "allowNull": false
                    },
                    "weight": {
                        "type": Sequelize.DECIMAL,
                        "field": "weight"
                    },
                    "createdAt": {
                        "type": Sequelize.DATE,
                        "field": "createdAt",
                        "allowNull": false
                    },
                    "updatedAt": {
                        "type": Sequelize.DATE,
                        "field": "updatedAt",
                        "allowNull": false
                    },
                    "petId": {
                        "type": Sequelize.INTEGER,
                        "field": "petId",
                        "onUpdate": "CASCADE",
                        "onDelete": "cascade",
                        "references": {
                            "model": "Pet",
                            "key": "id"
                        },
                        "allowNull": true
                    }
                },
                {
                    "charset": "utf8",
                    "transaction": transaction
                }
            ]
        }
    ];
};
var rollbackCommands = function(transaction) {
    return [{
            fn: "dropTable",
            params: ["DiaryPet", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["Feedback", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["ImagePet", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["Notification", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["Pet", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["Post", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["PremiumUser", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["Schedule", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["ScheduleCategory", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["ScheduleJob", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["Task", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["User", {
                transaction: transaction
            }]
        },
        {
            fn: "dropTable",
            params: ["WeightPet", {
                transaction: transaction
            }]
        }
    ];
};

module.exports = {
    pos: 0,
    useTransaction: true,
    execute: function(queryInterface, Sequelize, _commands)
    {
        var index = this.pos;
        function run(transaction) {
            const commands = _commands(transaction);
            return new Promise(function(resolve, reject) {
                function next() {
                    if (index < commands.length)
                    {
                        let command = commands[index];
                        console.log("[#"+index+"] execute: " + command.fn);
                        index++;
                        queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                    }
                    else
                        resolve();
                }
                next();
            });
        }
        if (this.useTransaction) {
            return queryInterface.sequelize.transaction(run);
        } else {
            return run(null);
        }
    },
    up: function(queryInterface, Sequelize)
    {
        return this.execute(queryInterface, Sequelize, migrationCommands);
    },
    down: function(queryInterface, Sequelize)
    {
        return this.execute(queryInterface, Sequelize, rollbackCommands);
    },
    info: info
};
