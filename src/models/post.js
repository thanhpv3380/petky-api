module.exports = (sequelize, Sequelize) => {
  const Post = sequelize.define('Post', {
    imageUrl: {
      type: Sequelize.STRING(100),
      field: 'imageUrl',
    },
    link: {
      type: Sequelize.STRING(100),
      field: 'link',
    },
  });
  return Post;
};
