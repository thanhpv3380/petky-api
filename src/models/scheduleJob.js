module.exports = (sequelize, Sequelize) => {
  const ScheduleJob = sequelize.define('ScheduleJob', {
    label: {
      type: Sequelize.STRING(100),
      field: 'label',
    },
    icon: {
      type: Sequelize.STRING(100),
      field: 'icon',
    },
    bgColor: {
      type: Sequelize.STRING(100),
      field: 'bgColor',
    },
    isDefault: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      field: 'isDefault',
    },
  });
  return ScheduleJob;
};
