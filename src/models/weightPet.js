module.exports = (sequelize, Sequelize) => {
  const WeightPet = sequelize.define('WeightPet', {
    weight: {
      type: Sequelize.DECIMAL,
      field: 'weight',
    },
  });
  return WeightPet;
};
