const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const taskDao = require('../daos/task');
const { taskTimeTypes } = require('../enums/task');
const { cronJobTask, destroyCronJob } = require('../utils/cron');

const findAll = async ({ query, search, sort, fields, page, size }) => {
  const data = await taskDao.findAll({
    searchFields: ['note'],
    query,
    search,
    sort,
    fields,
    page,
    size,
  });

  return data;
};

const findById = async (id) => {
  const task = await taskDao.findByCondition(id);
  if (!task) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return task;
};

const create = async (petId, data) => {
  const { timeType, days, weekdays } = data;
  if (timeType === taskTimeTypes.MONTHLY && (!days || days.length <= 0)) {
    throw new CustomError(errorCodes.DAYS_NOT_FOUND);
  }

  if (
    timeType === taskTimeTypes.WEEKLY &&
    (!weekdays || weekdays.length <= 0)
  ) {
    throw new CustomError(errorCodes.WEEKDAYS_NOT_FOUND);
  }

  const newTask = await taskDao.create({
    petId,
    ...data,
  });
  cronJobTask(newTask);
  return newTask;
};

const update = async (id, data) => {
  const task = await taskDao.findByCondition(id);
  if (!task) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }

  const newTask = {
    ...task,
    ...data,
  };
  const { timeType, days, weekdays } = newTask;

  if (timeType === taskTimeTypes.MONTHLY && (!days || days.length <= 0)) {
    throw new CustomError(errorCodes.DAYS_NOT_FOUND);
  }

  if (
    timeType === taskTimeTypes.WEEKLY &&
    (!weekdays || weekdays.length <= 0)
  ) {
    throw new CustomError(errorCodes.WEEKDAYS_NOT_FOUND);
  }

  await taskDao.update(id, data);
  destroyCronJob(id);
  cronJobTask(newTask);
};

const remove = async (id) => {
  destroyCronJob(id);
  await taskDao.remove(id);
};

module.exports = { findAll, findById, create, update, remove };
