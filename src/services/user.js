const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const userDao = require('../daos/user');

const findAll = async (query) => {
  const users = await userDao.findAllUser({ query });
  return users;
};

const findById = async (id) => {
  const user = await userDao.findUser(id);
  if (!user) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  delete user.password;
  return user;
};

const update = async (id, data) => {
  const user = await userDao.findUser(id);
  if (!user) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await userDao.updateUser(id, data);
};

const remove = async (id) => {
  await userDao.deleteUser(id);
};

module.exports = { findAll, findById, update, remove };
