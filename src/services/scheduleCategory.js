const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const scheduleCategoryDao = require('../daos/scheduleCategory');

const findAll = async ({ userId }) => {
  const scheduleCategories = await scheduleCategoryDao.findAll({
    userId,
  });
  return scheduleCategories;
};

const findById = async (id) => {
  const scheduleCategory = await scheduleCategoryDao.findByCondition(id);
  if (!scheduleCategory) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return scheduleCategory;
};

const create = async (userId, data) => {
  const scheduleCategory = await scheduleCategoryDao.create({
    userId,
    ...data,
  });
  return scheduleCategory;
};

const update = async (id, data) => {
  const scheduleCategory = await scheduleCategoryDao.findByCondition(id);
  if (!scheduleCategory) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await scheduleCategoryDao.update(id, data);
};

const remove = async (id) => {
  await scheduleCategoryDao.remove(id);
};
module.exports = { findAll, findById, create, update, remove };
