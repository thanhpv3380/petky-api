const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const diaryPetDao = require('../daos/diaryPet');

const findAll = async ({ query, search, sort, fields, page, size }) => {
  const data = await diaryPetDao.findAll({
    searchFields: ['content'],
    query,
    search,
    sort,
    fields,
    page,
    size,
  });

  return data;
};

const findById = async (id) => {
  const diaryPet = await diaryPetDao.findByCondition(id);
  if (!diaryPet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return diaryPet;
};

const create = async (petId, data) => {
  const diaryPet = await diaryPetDao.create({
    petId,
    ...data,
  });
  return diaryPet;
};

const update = async (id, data) => {
  const diaryPet = await diaryPetDao.findByCondition(id);
  if (!diaryPet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await diaryPetDao.update(id, data);
};

const remove = async (id) => {
  await diaryPetDao.remove(id);
};
module.exports = { findAll, findById, create, update, remove };
