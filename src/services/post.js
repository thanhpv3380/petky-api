const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const postDao = require('../daos/post');

const findAll = async () => {
  const posts = await postDao.findAll();
  return posts;
};

const create = async (data) => {
  const post = await postDao.create(data);
  return post;
};

const update = async (id, data) => {
  const post = await postDao.findByCondition(id);
  if (!post) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await postDao.update(id, data);
};

const remove = async (id) => {
  await postDao.remove(id);
};

module.exports = { findAll, create, update, remove };
