const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const scheduleDao = require('../daos/schedule');

const findAll = async ({ userId }) => {
  const schedules = await scheduleDao.findAll({
    userId,
  });
  return schedules;
};

const findById = async (id) => {
  const schedule = await scheduleDao.findByCondition(id);
  if (!schedule) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return schedule;
};

const create = async (userId, data) => {
  const schedule = await scheduleDao.create({
    userId,
    ...data,
  });
  return schedule;
};

const update = async (id, data) => {
  const schedule = await scheduleDao.findByCondition(id);
  if (!schedule) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await scheduleDao.update(id, data);
};

const remove = async (id) => {
  await scheduleDao.remove(id);
};
module.exports = { findAll, findById, create, update, remove };
