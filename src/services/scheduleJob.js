const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const scheduleJobDao = require('../daos/scheduleJob');
const scheduleDao = require('../daos/schedule');

const findAll = async ({ userId }) => {
  const scheduleJobs = await scheduleJobDao.findAll({
    userId,
  });
  return scheduleJobs;
};

const findAllByScheduleId = async (scheduleId) => {
  const scheduleJobs = await scheduleJobDao.findAllByScheduleId(scheduleId);
  return scheduleJobs;
};

const findById = async (id) => {
  const scheduleJob = await scheduleJobDao.findByCondition(id);
  if (!scheduleJob) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return scheduleJob;
};

const create = async (userId, data) => {
  const { label, bgImage, scheduleCategoryId, icon, bgColor } = data;
  const schedule = await scheduleDao.create({
    label,
    bgImage,
    userId,
    scheduleCategoryId,
  });
  const scheduleJob = await scheduleJobDao.create({
    userId,
    label,
    icon,
    bgColor,
    scheduleId: schedule.id,
    scheduleCategoryId,
  });
  return { schedule, scheduleJob };
};

const update = async (id, data) => {
  const scheduleJob = await scheduleJobDao.findByCondition(id);
  if (!scheduleJob) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await scheduleJobDao.update(id, data);
};

const remove = async (id) => {
  await scheduleJobDao.remove(id);
};
module.exports = {
  findAll,
  findById,
  create,
  update,
  remove,
  findAllByScheduleId,
};
