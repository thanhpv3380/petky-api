const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const imagePetDao = require('../daos/imagePet');

const findAll = async ({ query, search, sort, fields, page, size }) => {
  const data = await imagePetDao.findAll({
    query,
    search,
    sort,
    fields,
    page,
    size,
  });

  return data;
};

const findById = async (id) => {
  const imagePet = await imagePetDao.findByCondition(id);
  if (!imagePet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return imagePet;
};

const create = async (petId, data) => {
  const imagePet = await imagePetDao.create({
    petId,
    ...data,
  });
  return imagePet;
};

const update = async (id, data) => {
  const imagePet = await imagePetDao.findByCondition(id);
  if (!imagePet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await imagePetDao.update(id, data);
};

const remove = async (id) => {
  await imagePetDao.remove(id);
};
module.exports = { findAll, findById, create, update, remove };
