const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const petDao = require('../daos/pet');

const findAll = async (query) => {
  const pets = await petDao.findAll({ query });
  return pets;
};

const findById = async (id) => {
  const pet = await petDao.findByCondition(id);
  if (!pet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return pet;
};

const create = async (userId, data) => {
  const pet = await petDao.create({ userId, ...data });
  return pet;
};

const update = async (id, data) => {
  const pet = await petDao.findByCondition(id);
  if (!pet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await petDao.update(id, data);
};

const remove = async (id) => {
  await petDao.remove(id);
};
module.exports = { findAll, findById, create, update, remove };
