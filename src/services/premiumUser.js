const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const premiumUserDao = require('../daos/premiumUser');
const userDao = require('../daos/user');
const userTypes = require('../enums/user');

const findAll = async ({ query, search, sort, fields, page, size }) => {
  const data = await premiumUserDao.findAll({
    searchFields: ['phoneNumber'],
    query,
    search,
    sort,
    fields,
    page,
    size,
  });

  return data;
};

const findById = async (id) => {
  const premiumUser = await premiumUserDao.findByCondition(id);
  if (!premiumUser) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return premiumUser;
};

const create = async ({ phoneNumber }) => {
  const premiumUserExist = await premiumUserDao.findByCondition({
    phoneNumber,
  });
  if (premiumUserExist) {
    throw new CustomError(errorCodes.PHONE_NUMBER_EXIST);
  }
  const premiumUser = await premiumUserDao.create({ phoneNumber });
  await userDao.updateUserByCondition(
    { phoneNumber },
    { type: userTypes.PREMIUM },
  );
  return premiumUser;
};

const remove = async (id) => {
  const premiumUserExist = await premiumUserDao.findByCondition(id);
  if (premiumUserExist) {
    await premiumUserDao.remove(id);
    await userDao.updateUserByCondition(
      { phoneNumber: premiumUserExist.phoneNumber },
      { type: userTypes.BASIC },
    );
  }
};

module.exports = { findAll, findById, create, remove };
