const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const feedbackDao = require('../daos/feedback');

const findAll = async ({ query, search, sort, fields, page, size }) => {
  const data = await feedbackDao.findAll({
    searchFields: ['content'],
    query,
    search,
    sort,
    fields,
    page,
    size,
  });

  return data;
};

const findById = async (id) => {
  const feedback = await feedbackDao.findByCondition(id);
  if (!feedback) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return feedback;
};

const create = async (userId, data) => {
  const feedback = await feedbackDao.create({
    userId,
    ...data,
  });
  return feedback;
};

const remove = async (id) => {
  await feedbackDao.remove(id);
};

module.exports = { findAll, findById, create, remove };
