const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { OAuth2Client } = require('google-auth-library');
const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const userDao = require('../daos/user');

const { generateRandomString } = require('../utils/random');
const {
  JWT_SECRET_KEY,
  ANDROID_CLIENT_ID,
  IOS_CLIENT_ID,
} = require('../configs');

const GOOGLE_APP_ID = [ANDROID_CLIENT_ID, IOS_CLIENT_ID];

const client = new OAuth2Client(GOOGLE_APP_ID);
const generateAccessToken = async (userId) => {
  const accessToken = await jwt.sign({ userId }, JWT_SECRET_KEY, {});
  return accessToken;
};

const login = async (email, password) => {
  const user = await userDao.findUser({ email });
  if (!user) throw new CustomError(errorCodes.USER_NOT_FOUND);
  const isCorrectPassword = await compareBcrypt(password, user.password);
  if (!isCorrectPassword) throw new CustomError(errorCodes.WRONG_PASSWORD);

  const userId = user.id;
  const accessToken = await generateAccessToken(userId);
  delete user.password;
  return { user, accessToken };
};

const verifyAccessToken = async (accessToken) => {
  const data = await jwt.verify(accessToken, JWT_SECRET_KEY);
  const { userId } = data;
  if (!userId) throw new CustomError(errorCodes.UNAUTHORIZED);
  const user = await userDao.findUser(userId);
  if (!user) throw new CustomError(errorCodes.USER_NOT_FOUND);
  delete user.password;
  return user;
};

const generateSalt = (rounds) => {
  return bcrypt.genSaltSync(rounds);
};

const hashBcrypt = (text, salt) => {
  const hashedBcrypt = new Promise((resolve, reject) => {
    bcrypt.hash(text, salt, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });
  return hashedBcrypt;
};

const compareBcrypt = async (data, hashed) => {
  const isCorrect = await new Promise((resolve, reject) => {
    bcrypt.compare(data, hashed, (err, same) => {
      if (err) reject(err);
      resolve(same);
    });
  });
  return isCorrect;
};

const register = async ({ email, password, phoneNumber, ...data }) => {
  const userExist = await Promise.all([
    await userDao.findUser({ email }),
    await userDao.findUser({ phoneNumber }),
  ]);

  if (userExist[0]) throw new CustomError(errorCodes.EMAIL_EXIST);
  if (userExist[1]) throw new CustomError(errorCodes.PHONE_NUMBER_EXIST);

  const salt = generateSalt(10);
  password = password || generateRandomString(16);
  password = await hashBcrypt(password, salt);

  const user = await userDao.createUser({
    email,
    password,
    phoneNumber,
    ...data,
  });
  delete user.password;

  const accessToken = await generateAccessToken(user.id);
  delete user.password;
  return { user, accessToken };
};

const changePassword = async (id, oldPass, newPass) => {
  const user = await userDao.findUser(id);
  if (!user) throw new CustomError(errorCodes.USER_NOT_FOUND);
  if (!user.password) throw new CustomError(errorCodes.FORBIDDEN);
  const isCorrectPassword = await compareBcrypt(oldPass, user.password);
  if (!isCorrectPassword) throw new CustomError(errorCodes.WRONG_PASSWORD);

  const salt = generateSalt(10);
  let password = newPass || generateRandomString(16);
  password = await hashBcrypt(password, salt);

  await userDao.updateUser(id, { password });
};

const loginByGoogle = async (idToken) => {
  try {
    const res = await client.verifyIdToken({
      idToken,
      audience: GOOGLE_APP_ID,
    });
    const { email, picture: avatar, name, phoneNumber } = res.getPayload();
    const userExist = await userDao.findUser({ email });

    if (!userExist) {
      const user = await userDao.createUser({
        email,
        name,
        avatar,
        phoneNumber,
      });
      const accessToken = await generateAccessToken(user.id);
      return { user, accessToken };
    }
    const accessToken = await generateAccessToken(userExist.id);
    return { user: userExist, accessToken };
  } catch (error) {
    throw new CustomError(errorCodes.BAD_REQUEST);
  }
};

module.exports = {
  login,
  register,
  verifyAccessToken,
  changePassword,
  loginByGoogle,
};
