const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const weightPetDao = require('../daos/weightPet');
const petDao = require('../daos/pet');

const findAll = async ({ query, search, sort, fields, page, size }) => {
  const data = await weightPetDao.findAll({
    query,
    search,
    sort,
    fields,
    page,
    size,
  });

  return data;
};

const findById = async (id) => {
  const weightPet = await weightPetDao.findByCondition(id);
  if (!weightPet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return weightPet;
};

const create = async (petId, data) => {
  const weightPet = await weightPetDao.create({
    petId,
    ...data,
  });

  await petDao.update(petId, { weight: data.weight });
  return weightPet;
};

const update = async (id, data) => {
  const weightPet = await weightPetDao.findByCondition(id);
  if (!weightPet) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await weightPetDao.update(id, data);
};

const remove = async (id) => {
  await weightPetDao.remove(id);
};
module.exports = { findAll, findById, create, update, remove };
