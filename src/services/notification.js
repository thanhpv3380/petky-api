const CustomError = require('../errors/CustomError');
const errorCodes = require('../errors/code');

const notificationDao = require('../daos/notification');

const findAll = async (query) => {
  const notifications = await notificationDao.findAll({ query });
  return notifications;
};

const findById = async (id) => {
  const notification = await notificationDao.findByCondition(id);
  if (!notification) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  return notification;
};

const create = async (userId, data) => {
  const notification = await notificationDao.create({ userId, ...data });
  return notification;
};

const update = async (id, data) => {
  const notification = await notificationDao.findByCondition(id);
  if (!notification) {
    throw new CustomError(errorCodes.NOT_FOUND);
  }
  await notificationDao.update(id, data);
};

const removeAllByExpoPushToken = async (expoPushToken) => {
  await notificationDao.removeByCondition({ expoPushToken });
};

const remove = async (id) => {
  await notificationDao.removeByCondition({ id });
};

module.exports = {
  findAll,
  findById,
  create,
  update,
  remove,
  removeAllByExpoPushToken,
};
