const codes = require('./code');

const getErrorMessage = (code) => {
  switch (code) {
    case codes.USER_NOT_FOUND:
      return 'User not found';
    case codes.WRONG_PASSWORD:
      return 'Wrong password';
    case codes.EMAIL_EXIST:
      return 'Email exist';
    case codes.UNAUTHORIZED_ADD_PET:
      return "Account is basic, can't add pet";
    case codes.LIMIT_PET:
      return `Only max ${process.env.AMOUNT_PET_LIMITED || 5} pet`;
    case codes.UNAUTHORIZED_ADD_SCHEDULE:
      return "Account is basic, can't add schedule";
    case codes.PHONE_NUMBER_EXIST:
      return 'PhoneNumber exist';
    default:
      return null;
  }
};

module.exports = getErrorMessage;
