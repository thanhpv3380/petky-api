require('dotenv').config();

const {
  PORT,
  JWT_SECRET_KEY,
  JWT_EXPIRES_TIME,
  NODE_ENV,
  DOCUMENT_PORT,
  DOMAIN_NAME,

  DB_USERNAME,
  DB_PASSWORD,
  DB_DATABASE,
  DB_HOST,
  DB_PORT,
  FILE_SIZE_LIMITED,
  ANDROID_CLIENT_ID,
  IOS_CLIENT_ID,
} = process.env;

const Sequelize = require('sequelize');
const { A_WEEK } = require('../constants');

const MB = 1024 * 1024;

module.exports = {
  DOMAIN_NAME,
  ANDROID_CLIENT_ID,
  IOS_CLIENT_ID,
  PORT: parseInt(PORT, 10) || 3000,
  FILE_SIZE_LIMITED: parseInt(FILE_SIZE_LIMITED || 1, 10) * MB,
  JWT_SECRET_KEY,
  JWT_EXPIRES_TIME: parseInt(JWT_EXPIRES_TIME, 10) || A_WEEK,
  NODE_ENV: NODE_ENV || 'develop',
  DOCUMENT_PORT: parseInt(DOCUMENT_PORT, 10) || 3001,
  MYSQL: {
    username: DB_USERNAME || 'root',
    password: DB_PASSWORD || '1',
    database: DB_DATABASE || 'DB',
    host: DB_HOST || '127.0.0.1',
    port: DB_PORT || '3306',
    dialect: 'mysql',
    logging: false,
    define: {
      charset: 'utf8',
      collate: 'utf8_general_ci',
      freezeTableName: true,
      timestamps: true,
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    retry: {
      match: [
        Sequelize.ConnectionAcquireTimeoutError,
        Sequelize.ConnectionTimedOutError,
        /Deadlock/i,
      ],
      name: 'query',
      backoffBase: 100,
      backoffExponent: 1.1,
      timeout: 60000,
      max: Infinity,
    },
  },
};
