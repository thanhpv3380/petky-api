const scheduleCategoriesDefault = [
  {
    label: 'healthAndNutrition',
    bgImage: '',
    schedules: [
      {
        label: 'feeding',
        bgImage: 'feeding',
        scheduleJobs: [
          {
            label: 'feeding',
            icon: 'feeding',
            bgColor: '#D18B8B',
          },
        ],
      },
      {
        label: 'exercise',
        bgImage: 'exercise',
        scheduleJobs: [
          {
            label: 'exercise',
            icon: 'exercise',
            bgColor: '#D1B58B',
          },
        ],
      },
      {
        label: 'veterinary',
        bgImage: 'veterinary',
        scheduleJobs: [
          {
            label: 'vaccinate',
            icon: 'vaccinate',
            bgColor: '#A1D18B',
          },
          {
            label: 'medicalExamination',
            icon: 'vaccinate',
            bgColor: '#A1D18B',
          },
        ],
      },
    ],
  },
  {
    label: 'lifeStyle',
    bgImage: '',
    schedules: [
      {
        label: 'shower',
        bgImage: 'shower',
        scheduleJobs: [
          {
            label: 'shower',
            icon: 'shower',
            bgColor: '#8BBCD1',
          },
        ],
      },

      {
        label: 'spa',
        bgImage: 'spa',
        scheduleJobs: [
          {
            label: 'spa',
            icon: 'spa',
            bgColor: '#CC9EE2',
          },
        ],
      },
    ],
  },
];

module.exports = { scheduleCategoriesDefault };
