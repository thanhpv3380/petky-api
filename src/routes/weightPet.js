const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { getPet } = require('../middlewares/pet');
const weightPetController = require('../controllers/weightPet');

/**
 * @swagger
 * /getWeightsByPet:
 *   get:
 *     description: Find all
 *     tags:
 *      - weightPet
 *     security:
 *      - bearerAuth: []
 *     parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *      - in: query
 *        name: page
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 0
 *        description: page
 *      - in: query
 *        name: size
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: page size
 *      - in: query
 *        name: search
 *        required: false
 *        schema:
 *          type: string
 *        description: search key
 *      - in: query
 *        name: fields
 *        required: false
 *        schema:
 *          type: string
 *        description: fields select
 *      - in: query
 *        name: sort
 *        required: false
 *        schema:
 *          type: string
 *        description: sort by
 *     responses:
 *       200:
 *         description: Returns list weightPet by pet
 */
router.get(
  '/getWeightsByPet',
  auth,
  getPet,
  asyncMiddleware(weightPetController.findAllByPet),
);

/**
 * @openapi
 * /weightPets/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - weightPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the weightPet ID
 *    responses:
 *      200:
 *        description: Returns a weightPet.
 */
router.get(
  '/weightPets/:id',
  auth,
  asyncMiddleware(weightPetController.findById),
);

/**
 *  @swagger
 *  /weightPets:
 *  post:
 *    description: Create weightPet
 *    tags:
 *      - weightPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information weightPet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                weight:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns a weightPet information.
 */
router.post(
  '/weightPets',
  auth,
  getPet,
  asyncMiddleware(weightPetController.create),
);

/**
 *  @swagger
 *  /weightPets/{id}:
 *  put:
 *    description: Update weightPet
 *    tags:
 *      - weightPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the weightPet ID
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information weightPet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                weight:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/weightPets/:id',
  auth,
  getPet,
  asyncMiddleware(weightPetController.update),
);

/**
 *  @swagger
 *  /weightPets/{id}:
 *  delete:
 *    description: Delete weightPet
 *    tags:
 *      - weightPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the weightPet ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/weightPets/:id',
  auth,
  asyncMiddleware(weightPetController.remove),
);

module.exports = router;
