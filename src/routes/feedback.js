const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { createValidate } = require('../validations/feedback');
const feedbackController = require('../controllers/feedback');

/**
 * @swagger
 * /feedbacks:
 *   get:
 *     description: Find all
 *     tags:
 *      - feedback
 *     security:
 *      - bearerAuth: []
 *     parameters:
 *      - in: query
 *        name: page
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 0
 *        description: page
 *      - in: query
 *        name: size
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: page size
 *      - in: query
 *        name: search
 *        required: false
 *        schema:
 *          type: string
 *        description: search key
 *      - in: query
 *        name: fields
 *        required: false
 *        schema:
 *          type: string
 *        description: fields select
 *      - in: query
 *        name: sort
 *        required: false
 *        schema:
 *          type: string
 *        description: sort by
 *     responses:
 *       200:
 *         description: Returns list feedback by pet
 */
router.get('/feedbacks', auth, asyncMiddleware(feedbackController.findAll));

/**
 * @swagger
 * /feedbacks/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - feedback
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the feedback ID
 *    responses:
 *      200:
 *        description: Returns a feedback.
 */
router.get(
  '/feedbacks/:id',
  auth,
  asyncMiddleware(feedbackController.findById),
);

/**
 *  @swagger
 *  /feedbacks:
 *  post:
 *    description: Create feedback
 *    tags:
 *      - feedback
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information feedback
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                content:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a feedback information.
 */
router.post(
  '/feedbacks',
  auth,
  createValidate,
  asyncMiddleware(feedbackController.create),
);

/**
 *  @swagger
 *  /feedbacks/{id}:
 *  delete:
 *    description: Delete feedback
 *    tags:
 *      - feedback
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the feedback ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/feedbacks/:id',
  auth,
  asyncMiddleware(feedbackController.remove),
);

module.exports = router;
