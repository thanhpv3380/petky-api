const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { checkAdmin } = require('../middlewares/post');
const postController = require('../controllers/post');
const { createValidate, updateValidate } = require('../validations/post');

/**
 * @swagger
 * /posts:
 *   get:
 *     description: Find all
 *     tags:
 *      - post
 *     security:
 *      - bearerAuth: []
 *     responses:
 *       200:
 *         description: Returns list post
 */
router.get('/posts', auth, asyncMiddleware(postController.findAll));

/**
 *  @swagger
 *  /posts:
 *  post:
 *    description: Create post
 *    tags:
 *      - post
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information post
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                imageUrl:
 *                  type: string
 *                link:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a post information.
 */
router.post(
  '/posts',
  auth,
  checkAdmin,
  createValidate,
  asyncMiddleware(postController.create),
);

/**
 *  @swagger
 *  /posts/{id}:
 *  put:
 *    description: Update post
 *    tags:
 *      - post
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the post ID
 *    requestBody:
 *        description: information post
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                imageUrl:
 *                  type: string
 *                link:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/posts/:id',
  auth,
  checkAdmin,
  updateValidate,
  asyncMiddleware(postController.update),
);

/**
 *  @swagger
 *  /posts/{id}:
 *  delete:
 *    description: Delete post
 *    tags:
 *      - post
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the post ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/posts/:id',
  auth,
  checkAdmin,
  asyncMiddleware(postController.remove),
);

module.exports = router;
