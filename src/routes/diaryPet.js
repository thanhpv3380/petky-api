const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { getPet } = require('../middlewares/pet');
const diaryPetController = require('../controllers/diaryPet');

/**
 * @swagger
 * /getDiariesByPet:
 *   get:
 *     description: Find all
 *     tags:
 *      - diaryPet
 *     security:
 *      - bearerAuth: []
 *     parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *      - in: query
 *        name: page
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 0
 *        description: page
 *      - in: query
 *        name: size
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: page size
 *      - in: query
 *        name: search
 *        required: false
 *        schema:
 *          type: string
 *        description: search key
 *      - in: query
 *        name: fields
 *        required: false
 *        schema:
 *          type: string
 *        description: fields select
 *      - in: query
 *        name: sort
 *        required: false
 *        schema:
 *          type: string
 *        description: sort by
 *     responses:
 *       200:
 *         description: Returns list diaryPet by pet
 */
router.get(
  '/getDiariesByPet',
  auth,
  getPet,
  asyncMiddleware(diaryPetController.findAllByPet),
);

/**
 * @openapi
 * /diaryPets/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - diaryPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the diaryPet ID
 *    responses:
 *      200:
 *        description: Returns a diaryPet.
 */
router.get(
  '/diaryPets/:id',
  auth,
  asyncMiddleware(diaryPetController.findById),
);

/**
 *  @swagger
 *  /diaryPets:
 *  post:
 *    description: Create diaryPet
 *    tags:
 *      - diaryPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information diaryPet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                content:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a diaryPet information.
 */
router.post(
  '/diaryPets',
  auth,
  getPet,
  asyncMiddleware(diaryPetController.create),
);

/**
 *  @swagger
 *  /diaryPets/{id}:
 *  put:
 *    description: Update diaryPet
 *    tags:
 *      - diaryPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the diaryPet ID
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information diaryPet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                content:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/diaryPets/:id',
  auth,
  getPet,
  asyncMiddleware(diaryPetController.update),
);

/**
 *  @swagger
 *  /diaryPets/{id}:
 *  delete:
 *    description: Delete diaryPet
 *    tags:
 *      - diaryPet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the diaryPet ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/diaryPets/:id',
  auth,
  asyncMiddleware(diaryPetController.remove),
);

module.exports = router;
