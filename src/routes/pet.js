const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { addPetAuth } = require('../middlewares/pet');
const petController = require('../controllers/pet');
const { createValidate, updateValidate } = require('../validations/pet');
/**
 * @swagger
 * /getPetsByUser:
 *   get:
 *     description: Find all by user
 *     tags:
 *      - pet
 *     security:
 *      - bearerAuth: []
 *     responses:
 *       200:
 *         description: Returns list pet by user
 */
router.get(
  '/getPetsByUser',
  auth,
  asyncMiddleware(petController.findAllByUser),
);

/**
 * @openapi
 * /pets/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - pet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    responses:
 *      200:
 *        description: Returns a pet.
 */
router.get('/pets/:id', auth, asyncMiddleware(petController.findById));

/**
 *  @swagger
 *  /pets:
 *  post:
 *    description: Create pet
 *    tags:
 *      - pet
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information pet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                age:
 *                  type: number
 *                weight:
 *                  type: number
 *                gender:
 *                  type: string
 *                species:
 *                  type: string
 *                avatar:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a pet information.
 */
router.post(
  '/pets',
  auth,
  addPetAuth,
  createValidate,
  asyncMiddleware(petController.create),
);

/**
 *  @swagger
 *  /pets/{id}:
 *  put:
 *    description: Update pet
 *    tags:
 *      - pet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information pet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                age:
 *                  type: number
 *                weight:
 *                  type: number
 *                gender:
 *                  type: string
 *                species:
 *                  type: string
 *                avatar:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/pets/:id',
  auth,
  updateValidate,
  asyncMiddleware(petController.update),
);

/**
 *  @swagger
 *  /pets/{id}:
 *  delete:
 *    description: Delete pet
 *    tags:
 *      - pet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete('/pets/:id', auth, asyncMiddleware(petController.remove));

module.exports = router;
