const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const uploadController = require('../controllers/upload');

/* eslint-disable prettier/prettier */
/**
 * @openapi
 * /uploads/file:
 *  post:
 *    description: Upload single file
 *    tags:
 *      - upload
 *    requestBody:
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              file:
 *                type: string
 *                format: binary
 *    responses:
 *      200:
 *        description: Returns link image
 */
router.post('/uploads/file', asyncMiddleware(uploadController.uploadFile));
/* eslint-enable prettier/prettier */

module.exports = router;
