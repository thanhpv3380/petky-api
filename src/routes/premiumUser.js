const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { createValidate } = require('../validations/premiumUser');
const premiumUserController = require('../controllers/premiumUser');

/**
 * @swagger
 * /premiumUsers:
 *   get:
 *     description: Find all
 *     tags:
 *      - premiumUser
 *     security:
 *      - bearerAuth: []
 *     parameters:
 *      - in: query
 *        name: page
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 0
 *        description: page
 *      - in: query
 *        name: size
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: page size
 *      - in: query
 *        name: search
 *        required: false
 *        schema:
 *          type: string
 *        description: search key
 *      - in: query
 *        name: fields
 *        required: false
 *        schema:
 *          type: string
 *        description: fields select
 *      - in: query
 *        name: sort
 *        required: false
 *        schema:
 *          type: string
 *        description: sort by
 *     responses:
 *       200:
 *         description: Returns list premium user by pet
 */
router.get(
  '/premiumUsers',
  auth,
  asyncMiddleware(premiumUserController.findAll),
);

/**
 * @swagger
 * /premiumUsers/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - premiumUser
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the premiumUser ID
 *    responses:
 *      200:
 *        description: Returns a premiumUser.
 */
router.get(
  '/premiumUsers/:id',
  auth,
  asyncMiddleware(premiumUserController.findById),
);

/**
 *  @swagger
 *  /premiumUsers:
 *  post:
 *    description: Create premiumUser
 *    tags:
 *      - premiumUser
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information premiumUser
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                phoneNumber:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a premiumUser information.
 */
router.post(
  '/premiumUsers',
  auth,
  createValidate,
  asyncMiddleware(premiumUserController.create),
);

/**
 *  @swagger
 *  /premiumUsers/{id}:
 *  delete:
 *    description: Delete premiumUser
 *    tags:
 *      - premiumUser
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the premiumUser ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/premiumUsers/:id',
  auth,
  asyncMiddleware(premiumUserController.remove),
);

module.exports = router;
