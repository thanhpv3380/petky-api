const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const scheduleController = require('../controllers/schedule');
const { addScheduleAuth } = require('../middlewares/schedule');

/**
 * @swagger
 * /schedules:
 *   get:
 *     description: Find all
 *     tags:
 *      - schedule
 *     security:
 *      - bearerAuth: []
 *     responses:
 *       200:
 *         description: Returns list schedule by user
 */
router.get(
  '/schedules',
  auth,
  asyncMiddleware(scheduleController.findAllByUser),
);

/**
 * @openapi
 * /schedules/{id}/scheduleJobs:
 *  get:
 *    description: Find all schedule job by scheduleId
 *    tags:
 *      - schedule
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the schedule ID
 *    responses:
 *      200:
 *        description: Returns list schedule job.
 */
router.get(
  '/schedules/:id/scheduleJobs',
  auth,
  asyncMiddleware(scheduleController.findAllScheduleJob),
);

/**
 * @openapi
 * /schedules/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - schedule
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the schedule ID
 *    responses:
 *      200:
 *        description: Returns a schedule.
 */
router.get(
  '/schedules/:id',
  auth,
  asyncMiddleware(scheduleController.findById),
);

/**
 *  @swagger
 *  /schedules:
 *  post:
 *    description: Create schedule
 *    tags:
 *      - schedule
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information schedule
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                label:
 *                  type: string
 *                bgImage:
 *                  type: string
 *                scheduleCategoryId:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns a schedule information.
 */
router.post(
  '/schedules',
  auth,
  addScheduleAuth,
  asyncMiddleware(scheduleController.create),
);

/**
 *  @swagger
 *  /schedules/{id}:
 *  put:
 *    description: Update schedule
 *    tags:
 *      - schedule
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the schedule ID
 *    requestBody:
 *        description: information schedule
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                label:
 *                  type: string
 *                bgImage:
 *                  type: string
 *                scheduleCategoryId:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put('/schedules/:id', auth, asyncMiddleware(scheduleController.update));

/**
 *  @swagger
 *  /schedules/{id}:
 *  delete:
 *    description: Delete schedule
 *    tags:
 *      - schedule
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the schedule ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/schedules/:id',
  auth,
  asyncMiddleware(scheduleController.remove),
);

module.exports = router;
