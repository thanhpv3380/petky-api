const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { getPet } = require('../middlewares/pet');
const taskController = require('../controllers/task');
const { createValidate, updateValidate } = require('../validations/task');

/**
 * @swagger
 * /getTasksByPet:
 *   get:
 *     description: Find all
 *     tags:
 *      - task
 *     security:
 *      - bearerAuth: []
 *     parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *      - in: query
 *        name: page
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 0
 *        description: page
 *      - in: query
 *        name: size
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: page size
 *      - in: query
 *        name: search
 *        required: false
 *        schema:
 *          type: string
 *        description: search key
 *      - in: query
 *        name: fields
 *        required: false
 *        schema:
 *          type: string
 *        description: fields select
 *      - in: query
 *        name: sort
 *        required: false
 *        schema:
 *          type: string
 *        description: sort by
 *     responses:
 *       200:
 *         description: Returns list task by pet
 */
router.get(
  '/getTasksByPet',
  auth,
  getPet,
  asyncMiddleware(taskController.findAllByPet),
);

/**
 * @openapi
 * /tasks/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - task
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the task ID
 *    responses:
 *      200:
 *        description: Returns a task.
 */
router.get('/tasks/:id', auth, asyncMiddleware(taskController.findById));

/**
 *  @swagger
 *  /tasks:
 *  post:
 *    description: Create task
 *    tags:
 *      - task
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information task
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                dateTime:
 *                  type: string
 *                timeType:
 *                  type: string
 *                note:
 *                  type: string
 *                weekdays:
 *                  type: string
 *                days:
 *                  type: string
 *                scheduleJobId:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns a task information.
 */
router.post(
  '/tasks',
  auth,
  getPet,
  createValidate,
  asyncMiddleware(taskController.create),
);

/**
 *  @swagger
 *  /tasks/{id}:
 *  put:
 *    description: Update task
 *    tags:
 *      - task
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the task ID
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information task
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                dateTime:
 *                  type: string
 *                timeType:
 *                  type: string
 *                pending:
 *                  type: boolean
 *                note:
 *                  type: string
 *                weekdays:
 *                  type: string
 *                days:
 *                  type: string
 *                scheduleJobId:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/tasks/:id',
  auth,
  getPet,
  updateValidate,
  asyncMiddleware(taskController.update),
);

/**
 *  @swagger
 *  /tasks/{id}:
 *  delete:
 *    description: Delete task
 *    tags:
 *      - task
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the task ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete('/tasks/:id', auth, asyncMiddleware(taskController.remove));

module.exports = router;
