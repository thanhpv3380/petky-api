const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const notificationController = require('../controllers/notification');
const { createValidate } = require('../validations/notification');

/**
 *  @swagger
 *  /notifications:
 *  post:
 *    description: Create notification
 *    tags:
 *      - notification
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information notification
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                expoPushToken:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a notification information.
 */
router.post(
  '/notifications',
  auth,
  createValidate,
  asyncMiddleware(notificationController.create),
);

/**
 *  @swagger
 *  /notifications/expoPushToken:
 *  delete:
 *    description: Delete notification by expoPushToken
 *    tags:
 *      - notification
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information notification
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                expoPushToken:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/notifications/expoPushToken',
  auth,
  asyncMiddleware(notificationController.removeAllByExpoPushToken),
);

module.exports = router;
