const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const userController = require('../controllers/user');
const {
  updateProfileValidate,
  changePasswordValidate,
} = require('../validations/user');

/**
 * @swagger
 * /users:
 *   get:
 *     description: Find all
 *     tags:
 *      - user
 *     security:
 *      - bearerAuth: []
 *     responses:
 *       200:
 *         description: Returns list user
 */
router.get('/users', auth, asyncMiddleware(userController.findAll));

/**
 * @swagger
 * /users/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - user
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the user ID
 *    responses:
 *      200:
 *        description: Returns a user.
 */
router.get('/users/:id', auth, asyncMiddleware(userController.findById));

/**
 *  @swagger
 *  /users/updateProfile:
 *  put:
 *    description: Update user
 *    tags:
 *      - user
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information user
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                phoneNumber:
 *                  type: number
 *                address:
 *                  type: number
 *                type:
 *                  type: string
 *                avatar:
 *                  type: string
 *                expoPushToken:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/users/updateProfile',
  auth,
  updateProfileValidate,
  asyncMiddleware(userController.update),
);

/**
 *  @swagger
 *  /users/changePassword:
 *  put:
 *    description: Update user
 *    tags:
 *      - user
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: old pass, new pass
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                oldPass:
 *                  type: string
 *                newPass:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/users/changePassword',
  auth,
  changePasswordValidate,
  asyncMiddleware(userController.changePassword),
);

/**
 *  @swagger
 *  /users/deleteMe:
 *  delete:
 *    description: Delete user
 *    tags:
 *      - user
 *    security:
 *      - bearerAuth: []
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete('/users/deleteMe', auth, asyncMiddleware(userController.remove));

module.exports = router;
