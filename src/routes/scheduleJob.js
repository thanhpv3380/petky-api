const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { addScheduleAuth } = require('../middlewares/schedule');

const scheduleJobController = require('../controllers/scheduleJob');

/**
 * @swagger
 * /scheduleJobs:
 *   get:
 *     description: Find all
 *     tags:
 *      - scheduleJob
 *     security:
 *      - bearerAuth: []
 *     responses:
 *       200:
 *         description: Returns list scheduleJob by user
 */
router.get(
  '/scheduleJobs',
  auth,
  asyncMiddleware(scheduleJobController.findAllByUser),
);

/**
 * @openapi
 * /scheduleJobs/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - scheduleJob
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the scheduleJob ID
 *    responses:
 *      200:
 *        description: Returns a scheduleJob.
 */
router.get(
  '/scheduleJobs/:id',
  auth,
  asyncMiddleware(scheduleJobController.findById),
);

/**
 *  @swagger
 *  /scheduleJobs:
 *  post:
 *    description: Create scheduleJob
 *    tags:
 *      - scheduleJob
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information scheduleJob
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                label:
 *                  type: string
 *                icon:
 *                  type: string
 *                bgColor:
 *                  type: string
 *                bgImage:
 *                  type: string
 *                scheduleCategoryId:
 *                  type: number
 *    responses:
 *      200:
 *        description: Returns a scheduleJob information.
 */
router.post(
  '/scheduleJobs',
  auth,
  addScheduleAuth,
  asyncMiddleware(scheduleJobController.create),
);

/**
 *  @swagger
 *  /scheduleJobs/{id}:
 *  put:
 *    description: Update scheduleJob
 *    tags:
 *      - scheduleJob
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the scheduleJob ID
 *    requestBody:
 *        description: information scheduleJob
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                label:
 *                  type: string
 *                icon:
 *                  type: string
 *                bgColor:
 *                  type: string
 *                scheduleId:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/scheduleJobs/:id',
  auth,
  asyncMiddleware(scheduleJobController.update),
);

/**
 *  @swagger
 *  /scheduleJobs/{id}:
 *  delete:
 *    description: Delete scheduleJob
 *    tags:
 *      - scheduleJob
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the scheduleJob ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/scheduleJobs/:id',
  auth,
  asyncMiddleware(scheduleJobController.remove),
);

module.exports = router;
