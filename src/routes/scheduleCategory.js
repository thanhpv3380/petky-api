const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const scheduleCategoryController = require('../controllers/scheduleCategory');
const { addScheduleAuth } = require('../middlewares/schedule');

/**
 * @swagger
 * /scheduleCategories:
 *   get:
 *     description: Find all
 *     tags:
 *      - scheduleCategory
 *     security:
 *      - bearerAuth: []
 *     responses:
 *       200:
 *         description: Returns list scheduleCategory by user
 */
router.get(
  '/scheduleCategories',
  auth,
  asyncMiddleware(scheduleCategoryController.findAllByUser),
);

/**
 * @openapi
 * /scheduleCategories/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - scheduleCategory
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the scheduleCategory ID
 *    responses:
 *      200:
 *        description: Returns a scheduleCategory.
 */
router.get(
  '/scheduleCategories/:id',
  auth,
  asyncMiddleware(scheduleCategoryController.findById),
);

/**
 *  @swagger
 *  /scheduleCategories:
 *  post:
 *    description: Create scheduleCategory
 *    tags:
 *      - scheduleCategory
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *        description: information scheduleCategory
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                label:
 *                  type: string
 *                bgImage:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a scheduleCategory information.
 */
router.post(
  '/scheduleCategories',
  auth,
  addScheduleAuth,
  asyncMiddleware(scheduleCategoryController.create),
);

/**
 *  @swagger
 *  /scheduleCategories/{id}:
 *  put:
 *    description: Update scheduleCategory
 *    tags:
 *      - scheduleCategory
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the scheduleCategory ID
 *    requestBody:
 *        description: information scheduleCategory
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                label:
 *                  type: string
 *                bgImage:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns.
 */
router.put(
  '/scheduleCategories/:id',
  auth,
  asyncMiddleware(scheduleCategoryController.update),
);

/**
 *  @swagger
 *  /scheduleCategories/{id}:
 *  delete:
 *    description: Delete scheduleCategory
 *    tags:
 *      - scheduleCategory
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the scheduleCategory ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/scheduleCategories/:id',
  auth,
  asyncMiddleware(scheduleCategoryController.remove),
);

module.exports = router;
