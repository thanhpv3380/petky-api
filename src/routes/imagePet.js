const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async');
const { auth } = require('../middlewares/auth');
const { getPet } = require('../middlewares/pet');
const imagePetController = require('../controllers/imagePet');

/**
 * @swagger
 * /getImagesByPet:
 *   get:
 *     description: Find all
 *     tags:
 *      - imagePet
 *     security:
 *      - bearerAuth: []
 *     parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *      - in: query
 *        name: page
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 0
 *        description: page
 *      - in: query
 *        name: size
 *        required: false
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: page size
 *      - in: query
 *        name: search
 *        required: false
 *        schema:
 *          type: string
 *        description: search key
 *      - in: query
 *        name: fields
 *        required: false
 *        schema:
 *          type: string
 *        description: fields select
 *      - in: query
 *        name: sort
 *        required: false
 *        schema:
 *          type: string
 *        description: sort by
 *     responses:
 *       200:
 *         description: Returns list imagePet by pet
 */
router.get(
  '/getImagesByPet',
  auth,
  getPet,
  asyncMiddleware(imagePetController.findAllByPet),
);

/**
 * @openapi
 * /imagePets/{id}:
 *  get:
 *    description: Find by id
 *    tags:
 *      - imagePet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the imagePet ID
 *    responses:
 *      200:
 *        description: Returns a imagePet.
 */
router.get(
  '/imagePets/:id',
  auth,
  asyncMiddleware(imagePetController.findById),
);

/**
 *  @swagger
 *  /imagePets:
 *  post:
 *    description: Create imagePet
 *    tags:
 *      - imagePet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: header
 *        name: petId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the pet ID
 *    requestBody:
 *        description: information imagePet
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                url:
 *                  type: string
 *    responses:
 *      200:
 *        description: Returns a imagePet information.
 */
router.post(
  '/imagePets',
  auth,
  getPet,
  asyncMiddleware(imagePetController.create),
);

/**
 *  @swagger
 *  /imagePets/{id}:
 *  delete:
 *    description: Delete imagePet
 *    tags:
 *      - imagePet
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: the imagePet ID
 *    responses:
 *      200:
 *        description: Returns.
 */
router.delete(
  '/imagePets/:id',
  auth,
  asyncMiddleware(imagePetController.remove),
);

module.exports = router;
